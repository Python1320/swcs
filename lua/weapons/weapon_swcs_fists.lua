SWEP.Base = "weapon_swcs_knife"
SWEP.Category = "CS:GO"

DEFINE_BASECLASS(SWEP.Base)

SWEP.Slot = 0

SWEP.PrintName = "Bare Hands"
SWEP.Spawnable = true
SWEP.HoldType = "fist"

SWEP.ViewModel = "models/weapons/csgo/v_fists.mdl"

SWEP.ItemDefAttributes = [=["attributes 08/03/2020" {
    "primary clip size" "-1"
    "is full auto" "1"
    "recoil seed" "0"
    "recoil angle variance" "0"
    "recoil magnitude" "0"
    "recoil magnitude variance" "0"
    "recoil angle variance alt" "0"
    "recoil magnitude alt" "0"
    "recoil magnitude variance alt" "0"
}]=]
SWEP.ItemDefVisuals = [=["visuals 08/03/2020" {
}]=]

sound.Add({ name = "Gloves.Swish",
    channel = CHAN_WEAPON,
    volume = { 0.1, 0.3 },
    level = 100,
    pitch = { 98, 102 },
    sound = {
        Sound")physics/flesh/fist_swing_01.wav",
        Sound")physics/flesh/fist_swing_02.wav",
        Sound")physics/flesh/fist_swing_03.wav",
        Sound")physics/flesh/fist_swing_04.wav",
        Sound")physics/flesh/fist_swing_05.wav",
        Sound")physics/flesh/fist_swing_05.wav"
    }
})

sound.Add({ name = "Flesh.ImpactGloves",
    channel = CHAN_WEAPON,
    volume = { 0.2, 0.5},
    level = 100,
    pitch = { 98, 102 },
    sound = {
        Sound"physics/flesh/fist_hit_01.wav",
        Sound"physics/flesh/fist_hit_02.wav",
        Sound"physics/flesh/fist_hit_03.wav",
        Sound"physics/flesh/fist_hit_04.wav",
        Sound"physics/flesh/fist_hit_05.wav"
    }
})

sound.Add({ name = "Flesh.ImpactSoftGloves",
    channel = CHAN_WEAPON,
    volume = 1.0,
    level = 100,
    pitch = { 98, 103 },
    sound = {
        Sound"physics/body/body_impact_fists_01.wav",
        Sound"physics/body/body_impact_fists_02.wav",
        Sound"physics/body/body_impact_fists_03.wav",
        Sound"physics/body/body_impact_fists_04.wav",
        Sound"physics/body/body_impact_fists_05.wav",
    }
})


function SWEP:SetupDataTables()
    BaseClass.SetupDataTables(self)

    self:NetworkVar("Float", 16, "AttackDelay")
end

function SWEP:PrimaryAttack()
    if self:GetPlayerOwner() then
        self:GetPlayerOwner():LagCompensation(true)
    end

    self:Swing()

    if self:GetPlayerOwner() then
        self:GetPlayerOwner():LagCompensation(false)
    end
end

function SWEP:Swing()
    local owner = self:GetPlayerOwner()
    if not owner then return end

    local vForward = owner:GetAimVector()
    local vecSrc	= owner:GetShootPos()
    local vecEnd	= vecSrc + vForward * 48

    local tr = util.TraceLine({
        start = vecSrc,
        endpos = vecEnd,
        mask = MASK_SOLID,
        collisiongroup = COLLISION_GROUP_NONE,
        filter = owner
    })
    if not tr.Hit then
        util.TraceHull({
            start = vecSrc,
            endpos = vecEnd,
            mask = MASK_SOLID,
            collisiongroup = COLLISION_GROUP_NONE,
            filter = owner,
            mins = vector_origin,
            maxs = vector_origin,
            output = tr
        })
    end

    local bDidHit = tr.Fraction < 1

    local bFirstSwing = (self:GetNextPrimaryFire() + 0.4) < CurTime()
    if bFirstSwing then
        self:SetSwingLeft(true)
    end

    self:SetWeaponAnim(ACT_VM_HITCENTER)
    self:EmitSound("Gloves.Swish")

    local time = CurTime() + self:SequenceDuration()
    self:SetNextPrimaryFire(time)
    self:SetNextSecondaryFire(time)

    if bDidHit then
        local ent = tr.Entity

        local info = DamageInfo()
        info:SetInflictor(owner)
        info:SetAttacker(owner)
        info:SetDamage(15)
        info:SetDamageType(DMG_CLUB)
        info:SetDamagePosition(tr.HitPos)
        info:SetReportedPosition(tr.StartPos)

        local force = vForward:GetNormal() * GetConVar("phys_pushscale"):GetFloat()
        info:SetDamageForce(force)

        if SERVER and ent:IsPlayer() then
            ent:SetLastHitGroup(HITGROUP_GENERIC)
        end

        if SERVER then
            -- disable player pushback on bullet damage
            -- what the fuck
            if ent:IsPlayer() then
                owner:AddSolidFlags(FSOLID_TRIGGER)
            end

            ent:TakeDamageInfo(info)

            if ent:IsPlayer() then
                owner:RemoveSolidFlags(FSOLID_TRIGGER)
            end
        end

        if ent:IsValid() or ent == game.GetWorld() then
            if ( ent:IsPlayer() or ent:IsNPC() or ent:IsNextBot()  ) then
                self:EmitSound("Flesh.ImpactSoft")
            else
                self:EmitSound("Flesh.ImpactGloves")
            end
        end
    end

    owner:SetAnimation(PLAYER_ATTACK1)
end

function SWEP:SecondaryAttack()
    local owner = self:GetPlayerOwner()
    if not owner then return end

    self:SetWeaponSequence(self:LookupSequence("punch_hard"))
    owner:SetAnimation(PLAYER_ATTACK1)

    local time = CurTime() + self:SequenceDuration()
    self:SetNextPrimaryFire(time)
    self:SetNextSecondaryFire(time)

    self:SetAttackDelay(CurTime() + 0.55)
end

function SWEP:Think()
    BaseClass.Think(self)

    local owner = self:GetPlayerOwner()

    if self:GetAttackDelay() ~= 0 and self:GetAttackDelay() <= CurTime() then
        self:SetAttackDelay(0)

        if owner then
            owner:LagCompensation(true)

            local vForward = owner:GetAimVector()
            local vecSrc	= owner:GetShootPos()
            local vecEnd	= vecSrc + vForward * 48

            local tr = util.TraceLine({
                start = vecSrc,
                endpos = vecEnd,
                mask = MASK_SOLID,
                collisiongroup = COLLISION_GROUP_NONE,
                filter = owner
            })
            if not tr.Hit then
                util.TraceHull({
                    start = vecSrc,
                    endpos = vecEnd,
                    mask = MASK_SOLID,
                    collisiongroup = COLLISION_GROUP_NONE,
                    filter = owner,
                    mins = vector_origin,
                    maxs = vector_origin,
                    output = tr
                })
            end

            owner:LagCompensation(false)

            local bDidHit = tr.Fraction < 1

            self:EmitSound("Gloves.Swish")

            if bDidHit then
                local ent = tr.Entity

                local info = DamageInfo()
                info:SetInflictor(owner)
                info:SetAttacker(owner)
                info:SetDamage(30)
                info:SetDamageType(DMG_CLUB)
                info:SetDamagePosition(tr.HitPos)
                info:SetReportedPosition(tr.StartPos)

                local force = vForward:GetNormal() * GetConVar("phys_pushscale"):GetFloat()
                info:SetDamageForce(force)

                if SERVER and ent:IsPlayer() then
                    ent:SetLastHitGroup(HITGROUP_GENERIC)
                end

                if SERVER then
                    -- disable player pushback on bullet damage
                    -- what the fuck
                    if ent:IsPlayer() then
                        owner:AddSolidFlags(FSOLID_TRIGGER)
                    end

                    ent:TakeDamageInfo(info)

                    if ent:IsPlayer() then
                        owner:RemoveSolidFlags(FSOLID_TRIGGER)
                    end
                end

                if ent:IsValid() or ent == game.GetWorld() then
                    if ( ent:IsPlayer() or ent:IsNPC() or ent:IsNextBot() ) then
                        self:EmitSound("Flesh.ImpactSoft")
                    else
                        self:EmitSound("Flesh.ImpactSoftGloves")
                    end
                end
            end
        end
    end
end
