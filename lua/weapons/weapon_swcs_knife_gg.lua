SWEP.Base = "weapon_swcs_knife"
SWEP.Category = "CS:GO Knives"

SWEP.Slot = 0

SWEP.PrintName = "Knife (Gun Game)"
SWEP.Spawnable = true
SWEP.HoldType = "knife"

SWEP.WorldModel = Model"models/weapons/csgo/w_knife_gg.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_knife_gg.mdl"

SWEP.ItemDefAttributes = [=["attributes 08/03/2020" {
	"primary clip size" "-1"
	"is full auto" "1"
	"recoil seed" "0"
	"recoil angle variance" "0"
	"recoil magnitude" "0"
	"recoil magnitude variance" "0"
	"recoil angle variance alt" "0"
	"recoil magnitude alt" "0"
	"recoil magnitude variance alt" "0"
}]=]
SWEP.ItemDefVisuals = [=["visuals 08/03/2020" {
	"weapon_type" "knife"
}]=]

SWEP.IsKnife = true
SWEP.AutoSpawnable = false
