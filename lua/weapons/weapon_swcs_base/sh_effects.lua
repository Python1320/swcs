AddCSLuaFile()

local SECONDS_FOR_COOL_DOWN = 2.0
local MIN_TIME_BETWEEN_SMOKES = 4.0
local MAX_SMOKE_ATTACHMENT_INDEX = 16

SWEP.m_gunHeat = 0.0
SWEP.m_lastSmokeTime = 0.0
SWEP.m_smokeAttachments = 0

local BarrelSmoke = CreateClientConVar("swcs_fx_weapon_barrel_smoke", 1, true, false, "show smoke emitting from barrel after sustained fire")

function SWEP:UpdateGunHeat(heat, iAttachmentIndex, bDualGuns)
    if not BarrelSmoke:GetBool() then
        return end

    local currentShotTime = CurTime()
    local timeSinceLastShot = math.abs(currentShotTime - self:GetLastShotTime())

    -- Drain off any heat from prior shots.
    self.m_gunHeat = self.m_gunHeat - timeSinceLastShot * ( 1.0 / SECONDS_FOR_COOL_DOWN )
    if self.m_gunHeat <= 0.0 then
        self.m_gunHeat = 0.0
    end

    -- Add the new heat to the gun.
    self.m_gunHeat = self.m_gunHeat + heat
    if self.m_gunHeat > 1.0 then
        -- Reset the heat so we have to build up to it again.
        self.m_gunHeat = 0.0
        self.m_smokeAttachments = bit.bor(self.m_smokeAttachments, bit.lshift(0x1, iAttachmentIndex))
    end

    -- Logic for the gun smoke.
    -- We don't want to hammer the smoke effect too much, so prevent smoke from spawning too soon after the last smoke.
    if self.m_smokeAttachments ~= 0x0 and currentShotTime - self.m_lastSmokeTime > MIN_TIME_BETWEEN_SMOKES then
        local pszHeatEffect = self:GetHeatEffectName()

        if pszHeatEffect and #pszHeatEffect > 0 then
            local i = 1
            repeat
                local attachmentFlag = bit.lshift(0x1, i)

                if bit.band(attachmentFlag, self.m_smokeAttachments) > 0x0 then
                    -- Remove the attachment flag from the smoke attachments since we are firing it off.
                    self.m_smokeAttachments = bit.band(self.m_smokeAttachments, bit.bnot(attachmentFlag))

                    local owner = self:GetPlayerOwner()
                    if not owner then
                        continue end

                    local vm = owner:GetViewModel(self:ViewModelIndex())
                    if not vm:IsValid() then
                        continue end

                    -- Dispatch this effect to the split screens that are rendering this first person view model.
                    ParticleEffectAttach(pszHeatEffect, PATTACH_POINT_FOLLOW, vm, i)
                    self.m_lastSmokeTime = currentShotTime
                    break
                end

                i = i + 1
            until (i < MAX_SMOKE_ATTACHMENT_INDEX and self.m_smokeAttachments > 0x0)
        end

        -- Reset the smoke attachments so that we can start doing a smoke effect for later shots.
        self.m_smokeAttachments = 0x0
    end
end

function SWEP:GetMuzzleFlashEffect1stPerson()
    if not self.ItemVisuals then
        return "" end

    if self:GetHasSilencer() and self:GetSilencerOn() then
        return self.ItemVisuals.muzzle_flash_effect_1st_person_alt or ""
    else
        return self.ItemVisuals.muzzle_flash_effect_1st_person or ""
    end
end
function SWEP:GetMuzzleFlashEffect3rdPerson()
    if not self.ItemVisuals then
        return "" end

    if self:GetHasSilencer() and self:GetSilencerOn() then
        return self.ItemVisuals.muzzle_flash_effect_3rd_person_alt or ""
    else
        return self.ItemVisuals.muzzle_flash_effect_3rd_person or ""
    end
end

function SWEP:GetHeatEffectName()
    return self.ItemVisuals.heat_effect or ""
end

function SWEP:GetMuzzleAttachmentIndex_3rdPerson()
    if self:GetHasSilencer() and self:GetSilencerOn() then
        return self:LookupAttachment( "muzzle_flash2" )
    end

    return self:LookupAttachment( "muzzle_flash" )

end
function SWEP:GetMuzzleAttachmentIndex_1stPerson(vm)
    if IsValid(vm) then
        if self:GetHasSilencer() and self:GetSilencerOn() then
            return vm:LookupAttachment( "muzzle_flash2" )
        end

        return vm:LookupAttachment( "1" )
    end

    return -1
end
function SWEP:GetEjectBrassEffectName()
    if swcs.IsHalloween() then
        return "weapon_shell_casing_candycorn"
    end

    if not self.ItemVisuals then
        return "" end

    return self.ItemVisuals.eject_brass_effect or ""
end
function SWEP:GetEjectBrassAttachmentIndex_1stPerson(vm)
    if IsValid(vm) and isentity(vm) then
        return vm:LookupAttachment( "2" )
    end

    return -1
end
function SWEP:GetEjectBrassAttachmentIndex_3rdPerson()
    return self:LookupAttachment("shell_eject")
end

function SWEP:GetTracerOrigin()
    local owner = self:GetPlayerOwner()
    if not owner then
        return Vector() end

    local vm = owner:GetViewModel()
    if not vm:IsValid() then
        return owner:EyePos() end

    local bDrawPlayer = CLIENT and ((owner == LocalPlayer() and owner:ShouldDrawLocalPlayer()) or (owner == LocalPlayer():GetObserverTarget() and LocalPlayer():GetObserverMode() ~= OBS_MODE_IN_EYE)) or false
    local iAttachmentIndex = not bDrawPlayer and self:GetMuzzleAttachmentIndex_1stPerson(vm) or self:GetMuzzleAttachmentIndex_3rdPerson()

    local vecPos = owner:EyePos()

    local tAttachment = (bDrawPlayer and self or vm):GetAttachment(iAttachmentIndex)
    if tAttachment then
        vecPos:Set(tAttachment.Pos)
    end

    return vecPos
end

function SWEP:DoTracer(pszTracerEffectName, vecSrc, vecEnd)
    if SERVER and game.SinglePlayer() then return end

    if not (isstring(pszTracerEffectName) and #pszTracerEffectName > 0) then
        return --[[print("bad tracer effect", self, pszTracerEffectName)]] end

    if self:GetTracerFrequency() == 0 then
        return end

    local nBulletNumber = (self:GetMaxClip1() - self:Clip1()) + 1
    local iTracerFreq = self:GetTracerFrequency()

    if nBulletNumber % iTracerFreq ~= 0 then
        return end

    local owner = self:GetPlayerOwner()
    if not owner then
        return end

    local vm = owner:GetViewModel(self:ViewModelIndex())
    if not IsValid(vm) then
        return end

    vecSrc = self:GetTracerOrigin()

    -- trace to no intersection
    local tr = util.TraceLine({
        start = vecSrc + ((vecEnd - vecSrc):GetNormalized() * 2),
        endpos = vecEnd,
        mask = MASK_SHOT,
        filter = {self, owner},
        collisiongroup = COLLISION_GROUP_PROJECTILE
    })

    if tr.Fraction ~= 1.0 and owner then
        vecSrc = owner:EyePos()

        local vangles = owner:EyeAngles()
        local vforward, vright = vangles:Forward(), vangles:Right()

        vright:Mul(2.5)
        vforward:Mul(10)
        vecSrc:Add(vright)
        vecSrc:Add(vforward)
        vecSrc.z = vecSrc.z - 2.5
    end

    -- gmod seems to disregard particles with duplicate entity indexes
    -- so add on a random number to resolve that
    local iAttachment = -1
    if SERVER then
        iAttachment = self:GetMuzzleAttachmentIndex_3rdPerson() + 5
    end

    util.ParticleTracerEx(pszTracerEffectName, vecSrc, vecEnd, true, self:EntIndex(), iAttachment)
end

function SWEP:DoFireEffects()
    if SERVER and game.SinglePlayer() then return end

    local owner = self:GetPlayerOwner()
    if not owner then return end

    if owner:IsDormant() then return end
    if CLIENT then
        if owner == LocalPlayer() and not owner:ShouldDrawLocalPlayer() then return end
        if owner == LocalPlayer():GetObserverTarget() and LocalPlayer():GetObserverMode() == OBS_MODE_IN_EYE then return end
    end

    -- stupid fucking hack fix for gmod issue
    -- for remote players, the particles don't appear on the weapon correctly
    local iOffset = 0

    -- Muzzle Flash Effect.
    local iAttachmentIndex = self:GetMuzzleAttachmentIndex_3rdPerson()
    local pszEffect = self:GetMuzzleFlashEffect3rdPerson()
    if pszEffect and #pszEffect > 0 and iAttachmentIndex >= 0 and not self:GetNoDraw() then
        if SERVER then
            iOffset = 5
        end

        ParticleEffectAttach(pszEffect, PATTACH_POINT_FOLLOW, self, iAttachmentIndex + iOffset)
    end

    iOffset = 0
    -- Brass Eject Effect.
    iAttachmentIndex = self:GetEjectBrassAttachmentIndex_3rdPerson()
    pszEffect = self:GetEjectBrassEffectName()
    if pszEffect and #pszEffect > 0 and iAttachmentIndex >= 0 and not self:GetNoDraw() then
        if SERVER then
            iOffset = 6
        end

        ParticleEffectAttach(pszEffect, PATTACH_POINT_FOLLOW, self, iAttachmentIndex + iOffset)
    end
end
