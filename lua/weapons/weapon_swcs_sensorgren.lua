SWEP.Base = "weapon_swcs_base_grenade"
SWEP.Category = "CS:GO"

SWEP.Slot = 4

SWEP.Primary.Ammo = "swcs_tagrenade"

SWEP.PrintName = "Tactical Awareness Grenade"
SWEP.Spawnable = true
SWEP.WorldModel = Model"models/weapons/csgo/w_eq_sensorgrenade.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_sonar_bomb.mdl"

sound.Add({
    name = "Sensor.Activate",
    channel = CHAN_WEAPON,
    level = 65,
    volume = 0.5,
    pitch = 100,
    sound = Sound"weapons/csgo/sensorgrenade/sensor_arm.wav"
})
sound.Add({
    name = "Sensor.Equip",
    channel = CHAN_WEAPON,
    level = 65,
    volume = 0.4,
    pitch = 100,
    sound = Sound"weapons/csgo/sensorgrenade/sensor_equip.wav"
})
sound.Add({
    name = "Sensor.Land",
    channel = CHAN_STATIC,
    level = 65,
    volume = 0.7,
    pitch = 100,
    sound = Sound"weapons/csgo/sensorgrenade/sensor_land.wav"
})
sound.Add({
    name = "Sensor.WarmupBeep",
    channel = CHAN_STATIC,
    level = 75,
    volume = 0.3,
    pitch = 100,
    sound = Sound"weapons/csgo/sensorgrenade/sensor_detect.wav"
})
sound.Add({
    name = "Sensor.Detonate",
    channel = CHAN_STATIC,
    level = 140,
    volume = 1.0,
    pitch = 100,
    sound = Sound"weapons/csgo/sensorgrenade/sensor_explode.wav"
})
--[[

    "Sensor.DetectPlayer_Hud"
    {
        "channel"		"CHAN_STATIC"
        "volume"		"0.5"
        "pitch"			"PITCH_NORM"
        "soundlevel"  		"SNDLVL_65dB"
        "wave"			"~weapons/sensorgrenade/sensor_detecthud.wav"
    }
]]

SWEP.ItemDefAttributes = [=["attributes 09/03/2020" {
    "max player speed"		"245"
    "in game price"		"200"
    "crosshair min distance"		"7"
    "penetration"		"1"
    "damage"		"50"
    "range"		"4096"
    "range modifier"		"0.990000"
    "throw velocity"		"750.000000"
    "primary default clip size"		"1"
    "secondary default clip size"		"1"
    "weapon weight"		"1"
    "itemflag exhaustible"		"1"
    "max player speed alt"		"245"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/03/2020" {
    "weapon_type"		"Grenade"
    "player_animation_extension"		"gren"
    "primary_ammo"		"AMMO_TYPE_FLASHBANG"
    "sound_single_shot"		"SmokeGrenade_CSGO.Throw"
    "sound_nearlyempty"		"Default.nearlyempty"
}]=]

function SWEP:EmitGrenade()
    if SERVER then
        return ents.Create("swcs_sensorgrenade_projectile")
    else
        return NULL
    end
end