AddCSLuaFile()

if CLIENT or (SERVER and game.SinglePlayer()) then
	hook.Add("PlayerTick", "swcs.flashbang", function(ply,mv)
		ply:UpdateFlashBangEffect()
	end)
end

if CLIENT then
	local m_pFlashTexture
	local lastTimeGrabbed = 0.0
	local function PerformFlashbangEffect()
		local hFlashBangPlayer = LocalPlayer()

		local flAlphaScale = 1.0

		local overlaycolor = Color(255,255,255)

		-- draw the screenshot overlay portion of the flashbang effect
		local pMaterial = Material( "effects/flashbang" )
		if pMaterial then
			-- This is for handling split screen where we could potentially enter this function more than once a frame.
			-- Since this bit of code grabs both the left and right viewports of the buffer, it only needs to be done once per frame per flash.
			if ( CurTime() == lastTimeGrabbed ) then
				hFlashBangPlayer.m_bFlashScreenshotHasBeenGrabbed = true
			end

			--print(lastTimeGrabbed, CurTime(), hFlashBangPlayer.m_bFlashScreenshotHasBeenGrabbed)
			if ( not hFlashBangPlayer.m_bFlashScreenshotHasBeenGrabbed ) then
				local nScreenWidth, nScreenHeight = ScrW(), ScrH()

				-- update m_pFlashTexture
				lastTimeGrabbed = CurTime()

				m_pFlashTexture = GetRenderTarget( "_rt_FullFrameFB1", nScreenWidth, nScreenHeight )
				render.CopyRenderTargetToTexture(m_pFlashTexture)

				pMaterial:SetTexture("$basetexture", m_pFlashTexture)

				hFlashBangPlayer.m_bFlashScreenshotHasBeenGrabbed = true
			end

			if m_pFlashTexture then
				overlaycolor:SetUnpacked(
					hFlashBangPlayer:GetNWFloat("FlashOverlayAlpha", 0.0) * flAlphaScale,
					hFlashBangPlayer:GetNWFloat("FlashOverlayAlpha", 0.0) * flAlphaScale,
					hFlashBangPlayer:GetNWFloat("FlashOverlayAlpha", 0.0) * flAlphaScale
				)
				pMaterial:SetFloat("$alpha", overlaycolor.r / 255)

				render.SetMaterial(pMaterial)
				for pass = 1, 4 do
					render.DrawScreenQuadEx(0, 0, ScrW(), ScrH())
				end
			end
		end

		-- draw pure white overlay part of the flashbang effect.
		pMaterial = Material("effects/flashbang_white")
		if pMaterial then
			overlaycolor:SetUnpacked(
				hFlashBangPlayer:GetNWFloat("FlashOverlayAlpha", 0.0) * flAlphaScale,
				hFlashBangPlayer:GetNWFloat("FlashOverlayAlpha", 0.0) * flAlphaScale,
				hFlashBangPlayer:GetNWFloat("FlashOverlayAlpha", 0.0) * flAlphaScale
			)

			pMaterial:SetFloat("$alpha", overlaycolor.r / 255)
			render.SetMaterial(pMaterial)
			render.DrawScreenQuadEx(0, 0, ScrW(), ScrH())
		end
	end

	-- yea u can just remove these hooks and no more flashbangs
	-- but pls dont <3
	hook.Add("RenderScreenspaceEffects", "swcs.flashbang", function()
		PerformFlashbangEffect()
	end)
else
	hook.Add("DoPlayerDeath", "swcs.death", function(ply)
		ply:RemoveHelmet()
		ply:RemoveDefuser()

		local wep = ply:GetActiveWeapon()
		if wep.IsSWCSWeapon and wep.IsGrenade and wep:GetPinPulled() and not wep.m_bHasEmittedProjectile then
			wep:DropGrenade()
		end
	end)

	local rethrow_last_class = ""
	local rethrow_last_pos = Vector()
	local rethrow_last_vel = Vector()
	local rethrow_last_owner = NULL
	local rethrow_last_weapon_factory

	hook.Add("PlayerThrowSWCSGrenade", "swcs.sv_rethrow", function(ply, proj)
		rethrow_last_class = proj:GetClass()
		rethrow_last_pos = proj:GetPos()
		rethrow_last_vel = proj:GetInitialVelocity()
		rethrow_last_owner = ply
		rethrow_last_weapon_factory = proj.ItemAttributes
	end)

	concommand.Add("sv_rethrow_last_swcs_grenade", function()
		if rethrow_last_class == "" then
			return end

		local hEnt = ents.Create(rethrow_last_class)
		if not hEnt:IsValid() then
			return end

		hEnt.ItemAttributes = rethrow_last_weapon_factory
		hEnt:Create(rethrow_last_pos, angle_zero, rethrow_last_vel, Angle(600, math.Rand(-1200, 1200)), rethrow_last_owner)
		hEnt:Spawn()
	end, nil, "Emit the last grenade thrown on the server.", FCVAR_CHEAT)
end
