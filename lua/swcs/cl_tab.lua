hook.Add( "AddToolMenuCategories", "swcs.categories", function()
	spawnmenu.AddToolCategory("Utilities", "swcs", "SWCS")
end)

hook.Add("PopulateToolMenu", "swcs.tabs",function()

	-- sv toggles
	spawnmenu.AddToolMenuOption("Utilities", "swcs", "sv_settings", "Serverside Settings", "", "", function(pnl)
		pnl:ControlHelp("These require you to be admin to change.")
		pnl:CheckBox("Sync bullet spread seed", "swcs_weapon_sync_seed")
		pnl:CheckBox("Enable nospread", "weapon_accuracy_nospread")
		pnl:CheckBox("Individual weapon ammo", "swcs_weapon_individual_ammo")
		pnl:CheckBox("Shotgun spread patterns", "weapon_accuracy_shotgun_spread_patterns")

		pnl:NumSlider("Recoil scale", "weapon_recoil_scale", 0, 10, 1)
		pnl:NumSlider("Show bullet impacts", "sv_showimpacts", 0, 3, 0)
		pnl:NumSlider("Deploy speed multiplier", "swcs_deploy_override", 0, 4, 1)
	end)

	-- cl toggles
	spawnmenu.AddToolMenuOption("Utilities", "swcs", "cl_settings", "Clientside Settings", "", "", function(pnl)
		pnl:ControlHelp("Miscellaneous clientside settings.")

		pnl:CheckBox("Experimental Smoothing", "swcs_experm_interp")
		pnl:CheckBox("CS:GO Impact Effects", "swcs_fx_impact_style")
		pnl:CheckBox("Barrel Smoke", "swcs_fx_weapon_barrel_smoke")
	end)

	-- clientside shit
	spawnmenu.AddToolMenuOption("Utilities", "swcs", "xhair_settings", "Crosshair Settings", "", "", function(pnl)
		pnl:ControlHelp("Customize your crosshair.")

		local DrawBoard = vgui.Create("Panel", pnl)
		DrawBoard:Dock(TOP)
		function DrawBoard:PerformLayout()
			self:SetTall(pnl:GetWide())
		end

		-- crosshair preview
		do
			local cl_crosshairdot = GetConVar"cl_crosshairdot"
			local cl_crosshairstyle = GetConVar"cl_crosshairstyle"
			local cl_crosshaircolor = GetConVar"cl_crosshaircolor"
			local cl_crosshairalpha = GetConVar"cl_crosshairalpha"
			local cl_crosshaircolor_r = GetConVar"cl_crosshaircolor_r"
			local cl_crosshaircolor_g = GetConVar"cl_crosshaircolor_g"
			local cl_crosshaircolor_b = GetConVar"cl_crosshaircolor_b"
			local cl_crosshair_dynamic_splitdist = GetConVar"cl_crosshair_dynamic_splitdist"
			local cl_crosshairgap_useweaponvalue = GetConVar"cl_crosshairgap_useweaponvalue"
			local cl_crosshairgap = GetConVar"cl_crosshairgap"
			local cl_crosshairsize = GetConVar"cl_crosshairsize"
			local cl_crosshairthickness = GetConVar"cl_crosshairthickness"
			local cl_crosshair_dynamic_splitalpha_innermod = GetConVar"cl_crosshair_dynamic_splitalpha_innermod"
			local cl_crosshair_dynamic_splitalpha_outermod = GetConVar"cl_crosshair_dynamic_splitalpha_outermod"
			local cl_crosshair_dynamic_maxdist_splitratio = GetConVar"cl_crosshair_dynamic_maxdist_splitratio"
			local cl_crosshair_drawoutline = GetConVar"cl_crosshair_drawoutline"
			local cl_crosshair_outlinethickness = GetConVar"cl_crosshair_outlinethickness"
			local cl_crosshairusealpha = GetConVar"cl_crosshairusealpha"
			local cl_crosshair_t = GetConVar"cl_crosshair_t"

			local function DrawCrosshairRect(r,g,b,a, x0, y0, x1, y1, bAdditive)
				local w = math.max(x0, x1) - math.min(x0, x1)
				local h = math.max(y0, y1) - math.min(y0, y1)

				if cl_crosshair_drawoutline:GetBool() then
					local flThick = cl_crosshair_outlinethickness:GetFloat() * 2
					surface.SetDrawColor(0,0,0,a)
					surface.DrawRect(x0 - math.floor(flThick / 2), y0 - math.floor(flThick / 2), w + flThick, h + flThick)
				end

				surface.SetDrawColor(r,g,b,a)

				if bAdditive then
					surface.DrawTexturedRect(x0, y0, w, h)
				else
					surface.DrawRect(x0, y0, w, h)
				end
			end

			local SWITCH_CrosshairColor = {
				[0] = Color(250, 50, 50),
				Color(50, 250, 50),
				Color(250, 250, 50),
				Color(50, 50, 250),
				Color(50, 250, 250),
				function ()
					return Color(
						cl_crosshaircolor_r:GetInt(),
						cl_crosshaircolor_g:GetInt(),
						cl_crosshaircolor_b:GetInt()
					)
				end,
			}

			local function YRES(y)
				return y * (ScrH() / 480)
			end

			local bg = Material("hlmv/background")

			DrawBoard.m_flCrosshairDistance = 0
			function DrawBoard:Paint(w,h)
				surface.SetAlphaMultiplier(1)
				surface.SetDrawColor(255,255,255,255)
				surface.SetMaterial(bg)
				surface.DrawTexturedRect(0,0, w,h)

				local r,g,b = 50, 250, 50
				if SWITCH_CrosshairColor[cl_crosshaircolor:GetInt()] then
					local col = SWITCH_CrosshairColor[cl_crosshaircolor:GetInt()]

					if isfunction(col) then
						col = col()
					end

					r,g,b = col.r, col.g, col.b
				end

				local alpha = math.Clamp(cl_crosshairalpha:GetInt(), 0, 255)

				if not self.m_iCrosshairTextureID then
					self.m_iCrosshairTextureID = surface.GetTextureID("vgui/white_additive")
				end

				local bAdditive = not cl_crosshairusealpha:GetBool()
				if bAdditive then
					surface.SetTexture(self.m_iCrosshairTextureID)
					alpha = 200
				end

				local fHalfFov = math.rad(90) * 0.5
				local flInaccuracy = (math.abs(math.sin(RealTime())) * 0.1)
				local flSpread = 0

				local fSpreadDistance = ((flInaccuracy + flSpread) * 320 / math.tan(fHalfFov))
				local flCappedSpreadDistance = fSpreadDistance
				local flMaxCrossDistance = cl_crosshair_dynamic_splitdist:GetFloat()
				if fSpreadDistance > flMaxCrossDistance then
					flCappedSpreadDistance = flMaxCrossDistance
				end

				local iSpreadDistance = cl_crosshairstyle:GetInt() < 4 and math.floor(YRES(fSpreadDistance)) or 2
				local iCappedSpreadDistance = cl_crosshairstyle:GetInt() < 4 and math.floor(YRES(flCappedSpreadDistance)) or 2

				local fCrosshairDistanceGoal = cl_crosshairgap_useweaponvalue:GetBool() and 0 or 4 -- The minimum distance the crosshair can achieve...

				-- 0 = default
				-- 1 = default static
				-- 2 = classic standard
				-- 3 = classic dynamic
				-- 4 = classic static
				-- if ( cl_dynamiccrosshair.GetBool() )

				if self.m_flCrosshairDistance > fCrosshairDistanceGoal then
					if cl_crosshairstyle:GetInt() == 5 then
						self.m_flCrosshairDistance = self.m_flCrosshairDistance - 42 * FrameTime()
					else
						self.m_flCrosshairDistance = Lerp(FrameTime() / 0.025, fCrosshairDistanceGoal, self.m_flCrosshairDistance)
					end
				end

				-- clamp max crosshair expansion
				self.m_flCrosshairDistance = math.Clamp(self.m_flCrosshairDistance, fCrosshairDistanceGoal, 25.0)

				local iCrosshairDistance, iBarSize, iBarThickness
				local iCappedCrosshairDistance = 0

				iCrosshairDistance = math.floor((self.m_flCrosshairDistance * ScrH() / 1200.0) + cl_crosshairgap:GetFloat())
				iBarSize = math.floor( YRES( cl_crosshairsize:GetFloat() ))
				iBarThickness = math.max( 1, math.floor( YRES( cl_crosshairthickness:GetFloat() )) )

				-- 0 = default
				-- 1 = default static
				-- 2 = classic standard
				-- 3 = classic dynamic
				-- 4 = classic static
				-- if weapon_debug_spread_show:GetInt() == 2
				if iSpreadDistance > 0 and cl_crosshairstyle:GetInt() == 2 or cl_crosshairstyle:GetInt() == 3 then
					iCrosshairDistance = iSpreadDistance + cl_crosshairgap:GetFloat()

					if cl_crosshairstyle:GetInt() == 2 then
						iCappedCrosshairDistance = iCappedSpreadDistance + cl_crosshairgap:GetFloat()
					end
				elseif cl_crosshairstyle:GetInt() == 4 or (iSpreadDistance == 0 and (cl_crosshairstyle:GetInt() == 2 or cl_crosshairstyle:GetInt() == 3)) then
					iCrosshairDistance = fCrosshairDistanceGoal + cl_crosshairgap:GetFloat()
					iCappedCrosshairDistance = 4 + cl_crosshairgap:GetFloat()
				end

				local iCenterX = math.floor(w / 2)
				local iCenterY = math.floor(h / 2)

				-- 0 = default
				-- 1 = default static
				-- 2 = classic standard
				-- 3 = classic dynamic
				-- 4 = classic static

				local flAlphaSplitInner = cl_crosshair_dynamic_splitalpha_innermod:GetFloat()
				local flAlphaSplitOuter = cl_crosshair_dynamic_splitalpha_outermod:GetFloat()
				local flSplitRatio = cl_crosshair_dynamic_maxdist_splitratio:GetFloat()
				local iInnerCrossDist = iCrosshairDistance
				local flLineAlphaInner = alpha
				local flLineAlphaOuter = alpha
				local iBarSizeInner = iBarSize
				local iBarSizeOuter = iBarSize

				-- draw the crosshair that splits off from the main xhair
				if cl_crosshairstyle:GetInt() == 2 and fSpreadDistance > flMaxCrossDistance then
					iInnerCrossDist = iCappedCrosshairDistance
					flLineAlphaInner = alpha * flAlphaSplitInner
					flLineAlphaOuter = alpha * flAlphaSplitOuter
					iBarSizeInner = math.ceil(iBarSize * (1.0 - flSplitRatio))
					iBarSizeOuter = math.floor(iBarSize * flSplitRatio)

					-- draw horizontal crosshair lines
					local iInnerLeft = (iCenterX - iCrosshairDistance - iBarThickness / 2) - iBarSizeInner
					local iInnerRight = iInnerLeft + 2 * (iCrosshairDistance + iBarSizeInner) + iBarThickness
					local iOuterLeft = iInnerLeft - iBarSizeOuter
					local iOuterRight = iInnerRight + iBarSizeOuter
					local y0 = iCenterY - iBarThickness / 2
					local y1 = y0 + iBarThickness
					DrawCrosshairRect(r, g, b, flLineAlphaOuter, iOuterLeft, y0, iInnerLeft, y1, bAdditive)
					DrawCrosshairRect(r, g, b, flLineAlphaOuter, iInnerRight, y0, iOuterRight, y1, bAdditive)

					-- draw vertical crosshair lines
					local iInnerTop = (iCenterY - iCrosshairDistance - iBarThickness / 2) - iBarSizeInner
					local iInnerBottom = iInnerTop + 2 * (iCrosshairDistance + iBarSizeInner) + iBarThickness
					local iOuterTop = iInnerTop - iBarSizeOuter
					local iOuterBottom = iInnerBottom + iBarSizeOuter
					local x0 = iCenterX - iBarThickness / 2
					local x1 = x0 + iBarThickness
					if not cl_crosshair_t:GetBool() then DrawCrosshairRect(r, g, b, flLineAlphaOuter, x0, iOuterTop, x1, iInnerTop, bAdditive) end
					DrawCrosshairRect(r, g, b, flLineAlphaOuter, x0, iInnerBottom, x1, iOuterBottom, bAdditive)
				end

				-- draw horizontal crosshair lines
				local iInnerLeft = iCenterX - iInnerCrossDist - (iBarThickness / 2)
				local iInnerRight = iInnerLeft + (2 * iInnerCrossDist) + iBarThickness
				local iOuterLeft = iInnerLeft - iBarSizeInner
				local iOuterRight = iInnerRight + iBarSizeInner
				local y0 = iCenterY - (iBarThickness / 2)
				local y1 = y0 + iBarThickness
				DrawCrosshairRect(r, g, b, flLineAlphaInner, iOuterLeft, y0, iInnerLeft, y1, bAdditive)
				DrawCrosshairRect(r, g, b, flLineAlphaInner, iInnerRight, y0, iOuterRight, y1, bAdditive)

				-- draw vertical crosshair lines
				local iInnerTop = iCenterY - iInnerCrossDist - (iBarThickness / 2)
				local iInnerBottom = iInnerTop + (2 * iInnerCrossDist) + iBarThickness
				local iOuterTop = iInnerTop - iBarSizeInner
				local iOuterBottom = iInnerBottom + iBarSizeInner
				local x0 = iCenterX - (iBarThickness / 2)
				local x1 = x0 + iBarThickness
				if not cl_crosshair_t:GetBool() then DrawCrosshairRect(r, g, b, flLineAlphaInner, x0, iOuterTop, x1, iInnerTop, bAdditive) end
				DrawCrosshairRect(r, g, b, flLineAlphaInner, x0, iInnerBottom, x1, iOuterBottom, bAdditive)

				-- draw dot
				if cl_crosshairdot:GetBool() then
					local x0 = iCenterX - iBarThickness / 2
					local x1 = x0 + iBarThickness
					local y0 = iCenterY - iBarThickness / 2
					local y1 = y0 + iBarThickness
					DrawCrosshairRect(r, g, b, alpha, x0, y0, x1, y1, bAdditive)
				end
			end
		end

		pnl:CheckBox("Recoil on crosshair", "cl_crosshair_recoil")

		local style = pnl:ComboBox("Style", "cl_crosshairstyle")
		style:AddChoice("default", 0)
		style:AddChoice("default (static)", 1)
		style:AddChoice("accurate (split)", 2)
		style:AddChoice("accurate (dynamic)", 3)
		style:AddChoice("classic (static)", 4)
		style:AddChoice("classic (dynamic)", 5)

		local cmb_color = pnl:ComboBox("Color", "cl_crosshaircolor")
		cmb_color:AddChoice("red", 0)
		cmb_color:AddChoice("green", 1)
		cmb_color:AddChoice("yellow", 2)
		cmb_color:AddChoice("blue", 3)
		cmb_color:AddChoice("cyan", 4)
		cmb_color:AddChoice("custom", 5)
		pnl:NumSlider("Alpha value", "cl_crosshairalpha", 0, 255, 0)
		pnl:CheckBox("Use alpha", "cl_crosshairusealpha")

		local colormix = vgui.Create("DColorMixer", pnl)
		colormix:SetConVarR("cl_crosshaircolor_r")
		colormix:SetConVarG("cl_crosshaircolor_g")
		colormix:SetConVarB("cl_crosshaircolor_b")
		colormix:SetAlphaBar(false)
		pnl:AddItem(colormix)

		pnl:CheckBox("Tee-style crosshair", "cl_crosshair_t")
		pnl:CheckBox("Center dot", "cl_crosshairdot")
		pnl:NumSlider("Pip thickness", "cl_crosshairthickness", 0, 20, 0)
		pnl:NumSlider("Pip size", "cl_crosshairsize", 0, 250, 0)
		pnl:NumSlider("Center gap", "cl_crosshairgap", 0, 250, 0)
		pnl:CheckBox("Use weapon gap value", "cl_crosshairgap_useweaponvalue")
	end)

	-- viewmodel settings
	spawnmenu.AddToolMenuOption("Utilities", "swcs", "viewmodel", "Viewmodel Settings", "", "", function(pnl)
		pnl:ControlHelp("Affects how your viewmodel looks.")
		pnl:NumSlider("Offset X", "viewmodel_offset_x", -2.5, 2.5, 1)
		pnl:NumSlider("Offset Y", "viewmodel_offset_y", -2.5, 2.5, 1)
		pnl:NumSlider("Offset Z", "viewmodel_offset_z", -2.5, 2.5, 1)

		pnl:NumSlider("Recoil tracking", "viewmodel_recoil", 0.0, 1, 2)
		pnl:CheckBox("Use new headbob", "cl_use_new_headbob")
		pnl:NumSlider("Bob frequency", "cl_bobcycle", 0.1, 2.0, 2)
		pnl:NumSlider("Vertical bob amount", "cl_bobamt_vert", 0.1, 2.0, 1)
		pnl:NumSlider("Lateral bob amount", "cl_bobamt_lat", 0.1, 2.0, 1)
		pnl:NumSlider("Bob lower amount", "cl_bob_lower_amt", 5, 30, 1)

		--[[
			cl_viewmodel_shift_left_amt( "cl_viewmodel_shift_left_amt","1.5", FCVAR_ARCHIVE, "The amount the viewmodel shifts to the left when shooting accuracy increases.", true, 0.5, true, 2.0 );
			cl_viewmodel_shift_right_amt( "cl_viewmodel_shift_right_amt","0.75", FCVAR_ARCHIVE, "The amount the viewmodel shifts to the right when shooting accuracy decreases.", true, 0.25, true, 2.0 );
		]]
	end)

	-- hands
	spawnmenu.AddToolMenuOption("Utilities", "swcs", "hands", "Hands", "", "", function(pnl)
		pnl:ControlHelp("Affects how your hands look.")

		pnl:AddControl( "listbox", {label = "Hands", options = {
			["GMod Hands"] = {swcs_hands = 0},
			["Bare Hands"] = {swcs_hands = 1},
			["Anarchist Gloves"] = {swcs_hands = 2},
			["Ghost Hands"] = {swcs_hands = 3},
			["Bloodhound Gloves"] = {swcs_hands = 4},
			["Bloodhound Gloves (Broken Fang)"] = {swcs_hands = 5},
			["Bloodhound Gloves (Hydra)"] = {swcs_hands = 6},
			["Fingerless Gloves"] = {swcs_hands = 7},
			["Full-Finger Gloves"] = {swcs_hands = 8},
			["Leather Hand-Wrap Gloves"] = {swcs_hands = 9},
			["Hard-Knuckle Gloves"] = {swcs_hands = 10},
			["Hard-Knuckle Gloves (Black)"] = {swcs_hands = 11},
			["Hard-Knuckle Gloves (Blue)"] = {swcs_hands = 12},
			["Motorcycle Gloves"] = {swcs_hands = 13},
			["Slick Gloves"] = {swcs_hands = 14},
			["Specialist Gloves"] = {swcs_hands = 15},
			["Sporty Gloves"] = {swcs_hands = 16},
		}})

		pnl:AddControl( "slider", {label = "Skin Tone", command = "swcs_hands_skin", min = 0, max = 4})

		pnl:AddControl( "ComboBox", {label = "Sleeves", options = {
			["No Sleeves"] = {swcs_sleeves = 0},
			["Anarchist"] = {swcs_sleeves = 1},

			-- Balkan
			["Balkan (Old)"] = {swcs_sleeves = 2},
			["Balkan (New, variant 1)"] = {swcs_sleeves = 3},
			["Balkan (New, variant 2)"] = {swcs_sleeves = 4},
			["Balkan (New, variant 3)"] = {swcs_sleeves = 5},
			["Balkan (New, variant 4)"] = {swcs_sleeves = 6},

			-- Heavies
			["Heavy CT"] = {swcs_sleeves = 7},
			["Heavy T"] = {swcs_sleeves = 8},

			-- FBI
			["FBI (variant 1)"] = {swcs_sleeves = 9},
			["FBI (variant 2)"] = {swcs_sleeves = 10},
			["FBI (variant 3)"] = {swcs_sleeves = 11},
			["FBI (variant 4)"] = {swcs_sleeves = 12},

			-- hello ct
			["GIGN"] = {swcs_sleeves = 13},
			["GSG9"] = {swcs_sleeves = 14},
			["IDF"] = {swcs_sleeves = 15},

			-- DZ sleeve
			["Dangerzone Jumpsuit"] = {swcs_sleeves = 16},

			-- THE ROLEX
			["Gold Watch"] = {swcs_sleeves = 17},

			-- "Professional"
			["\"Professional\""] = {swcs_sleeves = 18},

			-- SAS
			["SAS"] = {swcs_sleeves = 19},
			["SAS Agent"] = {swcs_sleeves = 20},

			-- Separatist sleeve
			["Separatist"] = {swcs_sleeves = 21},

			-- Seal Team 6
			["Seal Team 6 (variant 1)"] = {swcs_sleeves = 22},
			["Seal Team 6 (variant 2)"] = {swcs_sleeves = 23},
			["Seal Team 6 (variant 3)"] = {swcs_sleeves = 24},
			["Seal Team 6 (variant 4)"] = {swcs_sleeves = 25},
			["Seal Team 6 (variant 5)"] = {swcs_sleeves = 26},
			["Seal Team 6 (variant 6)"] = {swcs_sleeves = 27},
			["Seal Team 6 (variant 7)"] = {swcs_sleeves = 28},
			["Seal Team 6 (variant 8)"] = {swcs_sleeves = 29},

			-- SWAT Team
			["SWAT (variant 1)"] = {swcs_sleeves = 30},
			["SWAT (variant 2)"] = {swcs_sleeves = 31},
			["SWAT (variant 3)"] = {swcs_sleeves = 32},

			-- i just got out the hospital
			["Wristband"] = {swcs_sleeves = 33},
		}})
	end)
end)

local CategoryMap = {
	["rifle"] = "Rifles",
	["shotgun"] = "Shotguns",
	["machinegun"] = "Machine Guns",
	["pistol"] = "Pistols",
	["sniperrifle"] = "Sniper Rifles",
	["submachinegun"] = "SMGs",
	["knife"] = "Knives",
	["grenade"] = "Grenades",
	["other"] = "Other",
}
hook.Add("PopulateWeapons", "swcs.creation_tab", function(pnlContent, tree, _)
	timer.Simple(0, function()
		-- Loop through the weapons and add them to the menu
		local Weapons = list.Get("Weapon")
		local Categorised = {}

		-- Build into categories
		for k, weapon in pairs( Weapons ) do
			if not weapon.Spawnable then continue end
			if not weapons.IsBasedOn(k, "weapon_swcs_base") or weapon.IsBaseWep then continue end

			local Category = weapon.Category or "Other2"
			if not isstring(Category) then Category = tostring(Category) end

			local swep = weapons.Get(weapon.ClassName)

			local SubCategory = "other"

			if swep and swep.ClassName ~= "weapon_swcs_taser" then
				local keyvals = util.KeyValuesToTable(swep.ItemDefVisuals or "")
				local strWeaponType = string.lower(keyvals.weapon_type or "")

				if strWeaponType ~= "" then
					SubCategory = strWeaponType
				end

				if not isstring(SubCategory) then
					SubCategory = tostring(SubCategory)
				end
			end

			Categorised[Category] = Categorised[Category] or {}
			Categorised[Category][SubCategory] = Categorised[Category][SubCategory] or {}
			table.insert(Categorised[Category][SubCategory], weapon)
		end

		-- Loop through each category
		for _, node in next, tree:Root():GetChildNodes() do
			if node:GetText() ~= "CS:GO" then continue end
			local SubCategories = Categorised[node:GetText()]

			if not SubCategories then continue end

			-- When we click on the node - populate it using this function
			node.DoPopulate = function( self )

				-- If we've already populated it - forget it.
				if ( self.PropPanel ) then return end

				-- Create the container panel
				self.PropPanel = vgui.Create( "ContentContainer", pnlContent )
				self.PropPanel:SetVisible( false )
				self.PropPanel:SetTriggerSpawnlistChange( false )

				for name, weps in SortedPairs(SubCategories) do
					if not table.IsEmpty(SubCategories) then
						local label = vgui.Create("ContentHeader", container)
						label:SetText(CategoryMap[name] or name)
						self.PropPanel:Add(label)
					end

					for k, ent in SortedPairsByMemberValue( weps, "PrintName" ) do
						spawnmenu.CreateContentIcon( ent.ScriptedEntityType or "weapon", self.PropPanel, {
							nicename	= ent.PrintName or ent.ClassName,
							spawnname	= ent.ClassName,
							material	= ent.IconOverride or "entities/" .. ent.ClassName .. ".png",
							admin		= ent.AdminOnly
						} )
					end
				end

			end

			-- If we click on the node populate it and switch to it.
			node.DoClick = function(self)
				self:DoPopulate()
				pnlContent:SwitchPanel(self.PropPanel)
			end
		end

		-- Select the first node
		local FirstNode = tree:Root():GetChildNode(0)
		if IsValid(FirstNode) then
			FirstNode:InternalDoClick()
		end
	end)
end)
