AddCSLuaFile()

game.AddParticles("particles/csgo/csgo_weapon_fx.pcf")
game.AddParticles("particles/csgo/csgo_impact_fx.pcf")
game.AddParticles("particles/csgo/csgo_blood_fx.pcf")
game.AddParticles("particles/csgo/explosions_fx.pcf")

PrecacheParticleSystem("weapon_shell_casing_candycorn")

PrecacheParticleSystem("explosion_basic")
PrecacheParticleSystem("explosion_basic_water")
PrecacheParticleSystem("explosion_hegrenade_dirt")
PrecacheParticleSystem("explosion_hegrenade_snow")
PrecacheParticleSystem("explosion_smokegrenade")
PrecacheParticleSystem("c4_train_ground_effect")
PrecacheParticleSystem("c4_timer_light")
PrecacheParticleSystem("c4_timer_light_trigger")

PrecacheParticleSystem("impact_wallbang_light")
PrecacheParticleSystem("impact_wallbang_heavy")

PrecacheParticleSystem("blood_impact_heavy")
PrecacheParticleSystem("blood_impact_medium")
PrecacheParticleSystem("blood_impact_light")
PrecacheParticleSystem("blood_impact_light_headshot")
PrecacheParticleSystem("impact_helmet_headshot")

PrecacheParticleSystem("impact_concrete_csgo")
PrecacheParticleSystem("impact_plaster_csgo")
PrecacheParticleSystem("impact_water_csgo")

PrecacheParticleSystem("weapon_snowball_trail")
PrecacheParticleSystem("weapon_snowball_impact")
PrecacheParticleSystem("weapon_snowball_impact_stuck_wall")
PrecacheParticleSystem("weapon_snowball_impact_splat")
PrecacheParticleSystem("snow_hit_player_screeneffect")

local swcs_debug_impact = CreateConVar("swcs_debug_impact", "0", {FCVAR_NOTIFY, FCVAR_REPLICATED}, "Show debug info for impact effects")
local TAG = "swcs_fx"

local FX_IMPACT_GENERIC = 0
local FX_IMPACT_BULLET = FX_IMPACT_GENERIC
local FX_IMPACT_KNIFE = 1
local FX_PARTICLE = 2

if SERVER then
    util.AddNetworkString(TAG)

    function swcs.SendParticle(particleName, vecPos, angles, bIsBlood, iBloodColor, entity, filter)
        if not filter then
            filter = RecipientFilter()
            filter:AddPVS(vecPos)
        end

        net.Start(TAG)
            net.WriteUInt(FX_PARTICLE, 3)

            net.WriteBool(bIsBlood)
            if bIsBlood then
                net.WriteInt(iBloodColor, 4)
            end

            net.WriteString(particleName)
            net.WriteVector(vecPos)
            net.WriteAngle(angles)
            net.WriteEntity(entity or NULL)
        net.Send(filter)
    end
else
    net.Receive(TAG, function()
        local type = net.ReadUInt(3)

        if type == FX_IMPACT_GENERIC or type == FX_IMPACT_KNIFE then
            local originEnt = net.ReadEntity()
            local vecOrigin = net.ReadVector()
            local vecStart = net.ReadVector()
            local iDamageType = net.ReadUInt(32)
            local iHitbox = net.ReadUInt(32)
            local pEntity = net.ReadEntity()
            local flags = net.ReadUInt(32)
            local nSurfaceProp = net.ReadUInt(8)

            local data = EffectData()
            data:SetOrigin(vecOrigin)
            data:SetStart(vecStart)
            data:SetDamageType(iDamageType)
            data:SetHitBox(iHitbox)
            data:SetEntity(pEntity)
            data:SetFlags(flags)
            data:SetSurfaceProp(nSurfaceProp)

            if type == FX_IMPACT_GENERIC then
                swcs.fx.ImpactEffect(data, originEnt)
            else
                swcs.fx.KnifeSlashEffect(data, originEnt)
            end
        elseif type == FX_PARTICLE then -- used to network confirmed bullet hits to weapon owner, and play particle effects
            local bIsBlood = net.ReadBool()
            if bIsBlood then
                local iBloodColor = net.ReadInt(4)

                if not swcs.fx.ShouldShowBlood(iBloodColor) then
                    return end
            end

            local strEffectName = net.ReadString()
            local vecOrigin = net.ReadVector()
            local angles = net.ReadAngle()
            local ent = net.ReadEntity()

            angles:Normalize()

            local pEffect
            if ent:IsValid() then
                pEffect = ent:CreateParticleEffect(strEffectName)
                swcs.fx.SetImpactControlPoint(pEffect, 0, vecOrigin, angle_zero, ent)
            else
                pEffect = CreateParticleSystemNoEntity(strEffectName, vecOrigin, angles)
            end

            --local forward, right, up = angles:Forward(), angles:Right(), angles:Up()
            --part:SetControlPoint(0, vecOrigin)
            --part:SetControlPointOrientation(0, forward,right,up)
            --part:SetControlPointEntity(0, ent)
        end
    end)
end

local ImpactStyle = CreateClientConVar("swcs_fx_impact_style", "0", true, false, "what style of bullet impact effects to use (0 = hl2, 1 = csgo)", 0, 1)
local damage_impact_heavy = CreateConVar( "swcs_fx_damage_impact_heavy", "40", nil, "Damage ABOVE this value is considered heavy damage" )
local damage_impact_medium = CreateConVar( "swcs_fx_damage_impact_medium", "20", nil, "Damage BELOW this value is considered light damage" )

swcs.fx = {}

local rawDecalData = Format("\"temp\" {\n%s\n}", file.Read("scripts/decals_subrect.txt", "GAME"))
local decalData = util.KeyValuesToTable(rawDecalData, true, true)

local engineDecalTranslation = {}
local csgoDecalTranslation = {
    ["C"] = "Impact_CSGO.Concrete",
    ["D"] = "Impact_CSGO.Dirt",
    --["D"] = "Impact_CSGO.Cardboard",
    ["F"] = "Impact_CSGO.Flesh",
    ["J"] = "Impact_CSGO.Snow",
    ["L"] = "Impact_CSGO.Plastic",
    ["M"] = "Impact_CSGO.Metal",
    ["N"] = "Impact_CSGO.Sand",
    ["O"] = "Impact_CSGO.Leaves",
    ["P"] = "Impact_CSGO.Computer",
    --["Q"] = "Impact_CSGO.Asphalt",
    --["R"] = "Impact_CSGO.Brick",
    ["T"] = "Impact_CSGO.Tile",
    ["U"] = "Impact_CSGO.Grass",
    ["V"] = "Impact_CSGO.Vent",
    ["W"] = "Impact_CSGO.Wood",
    ["Y"] = "Impact.Glass", --"Impact_CSGO.Glass",
}

-- decal registering
do
    game.AddDecal("Impact_CSGO.Concrete", {
        "decals/csgo/concrete/concrete1_subrect",
        "decals/csgo/concrete/concrete2_subrect",
        "decals/csgo/concrete/concrete3_subrect",
        "decals/csgo/concrete/concrete4_subrect"
    })
    game.AddDecal("Impact_CSGO.Dirt", {
        "decals/csgo/dirt/dirt1_subrect",
        "decals/csgo/dirt/dirt2_subrect",
        "decals/csgo/dirt/dirt3_subrect",
        "decals/csgo/dirt/dirt4_subrect"
    })
    game.AddDecal("Impact_CSGO.Grass", {
        "decals/csgo/dirt/dirt1_subrect",
        "decals/csgo/dirt/dirt2_subrect",
        "decals/csgo/dirt/dirt3_subrect",
        "decals/csgo/dirt/dirt4_subrect"
    })
    game.AddDecal("Impact_CSGO.Sand", {
        "decals/csgo/dirt/dirt1_subrect",
        "decals/csgo/dirt/dirt2_subrect",
        "decals/csgo/dirt/dirt3_subrect",
        "decals/csgo/dirt/dirt4_subrect"
    })
    game.AddDecal("Impact_CSGO.Snow", {
        "decals/csgo/dirt/dirt1_subrect",
        "decals/csgo/dirt/dirt2_subrect",
        "decals/csgo/dirt/dirt3_subrect",
        "decals/csgo/dirt/dirt4_subrect"
    })
    game.AddDecal("Impact_CSGO.Vent", {
        "decals/csgo/metal/metal01_subrect",
        "decals/csgo/metal/metal02_subrect",
        "decals/csgo/metal/metal03_subrect",
        "decals/csgo/metal/metal04_subrect"
    })
    game.AddDecal("Impact_CSGO.Metal", {
        "decals/csgo/metal/metal01_subrect",
        "decals/csgo/metal/metal02_subrect",
        "decals/csgo/metal/metal03_subrect",
        "decals/csgo/metal/metal04_subrect"
    })
    game.AddDecal("Impact_CSGO.Wood", {
        "decals/csgo/wood/wood1_subrect",
        "decals/csgo/wood/wood2_subrect",
        "decals/csgo/wood/wood3_subrect",
        "decals/csgo/wood/wood4_subrect"
    })
    game.AddDecal("Impact_CSGO.Plastic", {
        "decals/csgo/computer/computer1_subrect",
        "decals/csgo/computer/computer2_subrect",
        "decals/csgo/computer/computer3_subrect",
        "decals/csgo/computer/computer4_subrect"
    })
    game.AddDecal("Impact_CSGO.Computer", {
        "decals/csgo/computer/computer1_subrect",
        "decals/csgo/computer/computer2_subrect",
        "decals/csgo/computer/computer3_subrect",
        "decals/csgo/computer/computer4_subrect"
    })
    game.AddDecal("Impact_CSGO.Tile", {
        "decals/csgo/tile/tile1_subrect",
        "decals/csgo/tile/tile2_subrect",
        "decals/csgo/tile/tile3_subrect",
        "decals/csgo/tile/tile4_subrect",
        "decals/csgo/tile/tile5_subrect",
        "decals/csgo/tile/tile6_subrect",
    })
    game.AddDecal("Impact_CSGO.Flesh", {
        "decals/csgo/flesh/blood1_subrect",
        "decals/csgo/flesh/blood2_subrect",
        "decals/csgo/flesh/blood3_subrect",
        "decals/csgo/flesh/blood4_subrect",
        "decals/csgo/flesh/blood5_subrect",
        "decals/csgo/flesh/blood9_subrect",
    })
end

for charMat, matDecalName in next, decalData.TranslationData do
    engineDecalTranslation[string.byte(charMat, 1, 1)] = matDecalName
end

local keys = table.GetKeys(csgoDecalTranslation)
for _, charMat in ipairs(keys) do
    csgoDecalTranslation[string.byte(charMat, 1, 1)] = csgoDecalTranslation[charMat]
end

IMPACT_NODECAL = 0x1

function swcs.fx.ParseImpactData(data, vecOrigin, vecStart, vecShotDir)
    local nSurfaceProp, iMaterial, iDamageType, iHitbox

    local pEntity = data:GetEntity()
    if SERVER and not pEntity:IsValid() then
        pEntity = Entity(data:GetEntIndex())
    end

    vecOrigin:Set(data:GetOrigin())
    vecStart:Set(data:GetStart())
    vecShotDir:Set(vecOrigin - vecStart)
    vecShotDir:Normalize()

    local tr = util.TraceLine({
        start = vecStart,
        endpos = vecOrigin + vecShotDir * 1,
        mask = MASK_SHOT,
        filter = swcs.fx.CurrentPlayer
    })
    vecOrigin:Set(tr.HitPos)
    nSurfaceProp = tr.SurfaceProps

    iDamageType = data:GetDamageType()
    iHitbox = data:GetHitBox()

    local tSurfaceData = util.GetSurfaceData(nSurfaceProp)

    iMaterial = tSurfaceData and tSurfaceData.material or MAT_CONCRETE

    return pEntity, nSurfaceProp, iMaterial, iDamageType, iHitbox
end

function swcs.fx.GetImpactDecal(iMaterial, iDamageType)
    if iDamageType == DMG_SLASH then
        return "ManhackCut"
    end

    local translation = engineDecalTranslation
    if ImpactStyle:GetInt() == 1 then
        translation = csgoDecalTranslation
    end

    return translation[iMaterial] or "Impact.Concrete", translation[iMaterial] ~= nil
end

function swcs.fx.GetImpactMaterial(iMaterial, iDamageType)
    if SERVER then return end

    local decal, ok = swcs.fx.GetImpactDecal(iMaterial, iDamageType)
    local path = util.DecalMaterial(decal)
    if not path then
        if swcs_debug_impact:GetBool() then
            print("missing texture for impact decal", decal)
        end

        return nil, false
    end

    local mat = Material(path)

    if swcs_debug_impact:GetBool() then
        if not ok then
            print("missing decal for mat index", iMaterial)
        elseif mat:IsError() then
            print("material error for decal mat", path)
        end
    end

    return mat, ok
end

FX_WATER_IN_SLIME = 0x1
function swcs.fx.Impact(vecOrigin, vecStart, iMaterial, iDamageType, iHitbox, pEntity, tr, nFlags)
    -- Clear out the trace
    table.Empty(tr)
    tr.Fraction = 1

    -- Setup our shot information
    local shotDir = vecOrigin - vecStart
    local flLength = shotDir:Length()
    shotDir:Normalize()

    local traceExt = vecStart + (shotDir * (flLength + 8))

    util.TraceLine({
        start = vecStart,
        endpos = traceExt,
        mask = MASK_SHOT,
        output = tr,
        filter = {swcs.fx.CurrentPlayer}
    })

    -- fired into water
    if bit.band(util.PointContents(tr.HitPos), bit.bor(CONTENTS_WATER, CONTENTS_SLIME)) ~= 0 then
        local waterTrace = util.TraceLine({
            start = tr.StartPos,
            endpos = tr.HitPos,
            mask = bit.bor(CONTENTS_WATER,CONTENTS_SLIME),
            filter = {swcs.fx.CurrentPlayer},
            collisiongroup = COLLISION_GROUP_NONE,
        })

        if not waterTrace.AllSolid then
            local data = EffectData()
            data:SetOrigin(waterTrace.HitPos)
            data:SetNormal(waterTrace.HitNormal)
            data:SetScale(g_ursRandom:RandomFloat(8, 12))

            if bit.band(waterTrace.Contents, CONTENTS_SLIME) ~= 0 then
                data:SetFlags(bit.bor(data:GetFlags(), FX_WATER_IN_SLIME))
            else
                data:SetFlags(bit.bnot(FX_WATER_IN_SLIME))
            end

            if ImpactStyle:GetInt() == 0 then
                util.Effect("gunshotsplash", data, not game.SinglePlayer())
            else
                ParticleEffect("impact_water_csgo", waterTrace.HitPos, angle_zero, nil)
                sound.Play("water.bulletimpact", vecOrigin)
            end

            return false
        end
    end

    if bit.band(nFlags, IMPACT_NODECAL) == 0 then
        local decalMat, exists = swcs.fx.GetImpactMaterial(iMaterial, iDamageType)

        if not exists then
            return false end

        if decalMat then
            util.DecalEx(decalMat, pEntity, tr.HitPos, tr.HitNormal, color_white, 1, 1)
        end
    end

    if tr.Fraction == 1 then
        return false
    end

    return true
end

sound.Add({
    name = "FX_RicochetSound_CSGO.Ricochet",
    channel = CHAN_STATIC,
    level = 87,
    volume = 0.7,
    sound = { Sound("weapons/csgo/fx/rics/ric3.wav"), Sound("weapons/csgo/fx/rics/ric4.wav"), Sound("weapons/csgo/fx/rics/ric5.wav") },
})
sound.Add({
    name = "FX_RicochetSound_CSGO.Ricochet_Legacy",
    channel = CHAN_STATIC,
    level = 87,
    volume = 0.3,
    sound = { Sound("weapons/csgo/fx/rics/legacy_ric_conc-1.wav"), Sound("weapons/csgo/fx/rics/legacy_ric_conc-2.wav") },
})

function swcs.fx.RicochetSound(pos)
    local strSoundName = "FX_RicochetSound_CSGO.Ricochet"
    if g_ursRandom:RandomFloat() < 0.01 then -- 1% chance of playing legacy cs 1.6 ric sound.
        strSoundName = "FX_RicochetSound_CSGO.Ricochet_Legacy"
    end

    sound.Play(strSoundName, pos)
end

local SWITCH_RICO_CHANCE = {
    [MAT_METAL] = 5,
    [MAT_CONCRETE] = 5,
    [MAT_COMPUTER] = 5,
    [MAT_TILE] = 5,

    [MAT_GRATE] = 3,
    [MAT_VENT] = 3,
    [MAT_WOOD] = 3,

    [MAT_DIRT] = 1,
    [MAT_PLASTIC] = 1
}
function swcs.fx.PlayImpactSound(pEntity, tr, vecServerOrigin, nServerSurfaceProp, bDoRico)
    if bDoRico == nil then
        bDoRico = true
    end
    local vecOrigin = Vector()

    if pEntity:IsDormant() then
        return end

    -- If the client-side trace hit a different entity than the server, or
    -- the server didn't specify a surfaceprop, then use the client-side trace 
    -- material if it's valid.

    if tr.Hit and (pEntity ~= tr.Entity or nServerSurfaceProp == 0) then
        nServerSurfaceProp = tr.SurfaceProps
    end

    if nServerSurfaceProp == -1 then
        if swcs_debug_impact:GetBool() then
            print("missing surface data for surface prop", nServerSurfaceProp, "defaulting to concrete")
            print(Format("HIT: %s (%s)", tr.Entity:GetClass(), tr.Entity:GetModel()))
        end

        nServerSurfaceProp = util.GetSurfaceIndex("concrete")
    end

    local pdata = util.GetSurfaceData(nServerSurfaceProp)

    if tr.Fraction < 1 then
        vecOrigin:Set(tr.HitPos)
    else
        vecOrigin:Set(vecServerOrigin)
    end

    -- Now play the sound
    if pdata.bulletImpactSound then
        sound.Play(pdata.bulletImpactSound, vecOrigin)

        -- play a ricochet based on the material
        local flRicoChance = SWITCH_RICO_CHANCE[pdata.material] or 0
        if bDoRico and g_ursRandom:RandomFloat(0, 10) <= flRicoChance then
            swcs.fx.RicochetSound(vecOrigin)
        end
    end
end

local function VectorVectors( forward, right, up )
    local ang = forward:Angle()
    up:Set(ang:Up())
    right:Set(ang:Right())
end

swcs.fx.CurrentPlayer = NULL

function swcs.fx.SetImpactControlPoint( pEffect, nPoint, vecImpactPoint, vecForward, pEntity)
    local vecImpactY, vecImpactZ = Vector(), Vector()

    VectorVectors(vecForward, vecImpactY, vecImpactZ)

    pEffect:SetControlPoint(nPoint, vecImpactPoint)
    --pEffect:SetControlPointOrientation(nPoint, vecForward, vecImpactY, vecImpactZ)
    pEffect:SetControlPointOrientation(nPoint, vecImpactZ, vecImpactY, vecForward)
    pEffect:SetControlPointEntity(nPoint, pEntity)
end

--local r_drawflecks = GetConVar("r_drawflecks")
local noEffectsFlags = bit.bor(SURF_SKY, SURF_NODRAW, SURF_HINT, SURF_SKIP)
function swcs.fx.PerformCustomEffects(vecOrigin, tr, shotDir, iMaterial, iScale, nFlags)
    if bit.band(tr.SurfaceFlags, noEffectsFlags) ~= 0 then
        return end


    if not nFlags then
        nFlags = 0
    end

    --local bNoFlecks = not r_drawflecks:GetBool()
    --if not bNoFlecks then
    --    bNoFlecks = bit.band(nFlags, 0x1) ~= 0
    --end

    -- Compute the impact effect name
    local tSurfaceInfo = swcs.SurfaceInfo[swcs.SurfaceProps[tr.SurfaceProps]]
    local strImpactName

    if tSurfaceInfo then
        strImpactName = tSurfaceInfo.impact
    end

    if not strImpactName then
        if swcs_debug_impact:GetBool() then
            print("missing impact particle for surface prop", swcs.SurfaceProps[tr.SurfaceProps])
        end
        return
    end

    --print("impact", strImpactName)
    --[[
        const ImpactEffect_t &effect = pEffectList[ iMaterial - nOffset ];

        const char *pImpactName = effect.m_pName;
        int nEffectIndex = pEffectIndex[0];
        if ( bNoFlecks && effect.m_pNameNoFlecks )
        {
            pImpactName = effect.m_pNameNoFlecks;
            nEffectIndex = pEffectIndex[1];
        }
        if ( !pImpactName )
            return;
    ]]

    local flDot = shotDir:Dot(tr.HitNormal)
    local vecReflect = shotDir + (tr.HitNormal * (-2.0 * flDot))

    local vecShotBackward = -shotDir

    local vecImpactPoint = tr.Fraction ~= 1 and tr.HitPos or vecOrigin

    local pEffect = tr.Entity:CreateParticleEffect( strImpactName )
    if not (pEffect and pEffect:IsValid()) then
        return end

    swcs.fx.SetImpactControlPoint(pEffect, 0, vecImpactPoint, tr.HitNormal, tr.Entity)
    swcs.fx.SetImpactControlPoint(pEffect, 1, vecImpactPoint, vecReflect, tr.Entity)
    swcs.fx.SetImpactControlPoint(pEffect, 2, vecImpactPoint, vecShotBackward, tr.Entity)
    pEffect:SetControlPoint(3, Vector(iScale, iScale, iScale))

    local baseColor = render.GetSurfaceColor(tr.HitPos - tr.Normal * 1.1, tr.HitPos + tr.Normal)
    pEffect:SetControlPoint(4, baseColor)

    --baseColor = baseColor:ToColor()
    --debugoverlay.Line(tr.StartPos, tr.HitPos, 5, baseColor, true)
end

-- ImpactCallback
function swcs.fx.ImpactEffect(data, ply)
    -- hl2 style
    local tr = {}
    local vecOrigin, vecStart, vecShotDir = Vector(), Vector(), Vector()

    swcs.fx.CurrentPlayer = ply or NULL

    local pEntity, nSurfaceProp, iMaterial, iDamageType, iHitbox = swcs.fx.ParseImpactData(data, vecOrigin, vecStart, vecShotDir)

    if not IsValid(pEntity) and not pEntity:IsWorld() then
        return
    end

    local flags = 0
    if bit.band(iDamageType, DMG_SHOCK) ~= 0 then -- no decals for shock damage
        flags = IMPACT_NODECAL
    end

    if SERVER then
        sound.EmitHint(SOUND_BULLET_IMPACT, vecOrigin, 400, 0.2, ply)
        local filter = RecipientFilter()
        filter:AddPAS(vecOrigin)
        if not game.SinglePlayer() then
            filter:RemovePlayer(swcs.fx.CurrentPlayer)
        end

        net.Start(TAG, true)
            net.WriteUInt(FX_IMPACT_GENERIC, 3)
            net.WriteEntity(swcs.fx.CurrentPlayer)
            net.WriteVector(vecOrigin)
            net.WriteVector(vecStart)
            net.WriteUInt(iDamageType, 32)
            net.WriteUInt(iHitbox, 32)
            net.WriteEntity(pEntity)
            net.WriteUInt(flags, 32)
            net.WriteUInt(nSurfaceProp, 8)
        net.Send(filter)

        return
    end

    if swcs.fx.Impact(vecOrigin, vecStart, iMaterial, iDamageType, iHitbox, pEntity, tr, flags) then
        if ImpactStyle:GetInt() == 0 then
            util.Effect("Impact", data, not game.SinglePlayer())
            return
        end

        swcs.fx.PerformCustomEffects(vecOrigin, tr, vecShotDir, iMaterial, 1)
    end

    swcs.fx.PlayImpactSound(pEntity, tr, vecOrigin, nSurfaceProp)
end

-- KnifeSlash
function swcs.fx.KnifeSlashEffect(data, ply)
    if (CLIENT or game.SinglePlayer()) and ImpactStyle:GetInt() == 0 then
        --if ent:IsPlayer() or ent:IsNPC() then
        --	swcs.ImpactTrace(tr, DMG_GENERIC, owner)
        --else
        --	swcs.ImpactTrace(tr, DMG_SLASH, owner)
        --end

        if data:GetEntity():IsPlayer() then
            data:SetDamageType(DMG_GENERIC)
        end

        util.Effect("Impact", data, not game.SinglePlayer())
        return
    end

    local tr = {}
    local vecOrigin, vecStart, vecShotDir = Vector(), Vector(), Vector()
    local iMaterial, iDamageType, iHitbox = 0, 0, 0
    local nSurfaceProp = 0

    swcs.fx.CurrentPlayer = ply or NULL

    local pEntity
    pEntity, nSurfaceProp, iMaterial, iDamageType, iHitbox = swcs.fx.ParseImpactData(data, vecOrigin, vecStart, vecShotDir)

    if not IsValid(pEntity) and not pEntity:IsWorld() then
        return
    end

    local decal = util.DecalMaterial(swcs.fx.GetImpactDecal(iMaterial, iDamageType))
    if not decal then
        return end

    if SERVER then
        sound.EmitHint(SOUND_BULLET_IMPACT, vecOrigin, 400, 0.2, ply)
        local filter = RecipientFilter()
        filter:AddPAS(vecOrigin)
        if not game.SinglePlayer() then
            filter:RemovePlayer(swcs.fx.CurrentPlayer)
        end

        net.Start(TAG, true)
            net.WriteUInt(FX_IMPACT_GENERIC, 3)
            net.WriteEntity(swcs.fx.CurrentPlayer)
            net.WriteVector(vecOrigin)
            net.WriteVector(vecStart)
            net.WriteUInt(iDamageType, 32)
            net.WriteUInt(iHitbox, 32)
            net.WriteEntity(pEntity)
            net.WriteUInt(data:GetFlags(), 32)
            net.WriteUInt(nSurfaceProp, 8)
        net.Send(filter)

        return
    end

    local mat = Material(decal)
    if pEntity:IsWorld() then
        local shotDir = vecOrigin - vecStart
        local flLength = shotDir:Length()
        shotDir:Normalize()

        local traceExt = vecStart + (shotDir * (flLength + 8))

        util.TraceLine({
            start = vecStart,
            endpos = traceExt,
            mask = MASK_SHOT,
            output = tr,
            filter = {swcs.fx.CurrentPlayer}
        })

        local normal = Vector(tr.Normal)

        if tr.HitNormal.z == 0 then
            normal.z = 0
            normal:Normalize()
        end

        local planeRight = normal:Cross(-vector_up)
        local planeForward = planeRight:Cross(vector_up)

        local decalAngle = planeForward:AngleEx(vector_up)
        local decalNormal = decalAngle:Forward()

        if tr.HitNormal.z ~= 0 then
            decalNormal:Set(planeRight)
        end

        util.DecalEx(mat, pEntity, tr.HitPos, decalNormal, color_white, 1, 1)
    else
        --
    end

    if swcs.fx.Impact(vecOrigin, vecStart, iMaterial, iDamageType, iHitbox, pEntity, tr, data:GetFlags()) then
        swcs.fx.PerformCustomEffects(vecOrigin, tr, vecShotDir, iMaterial, 1)
    end

    swcs.fx.PlayImpactSound(pEntity, tr, vecOrigin, nSurfaceProp, false)
end

-- BloodSprayCallback "csblood"
function swcs.fx.BloodSpray( data, owner )
    local origin = data:GetOrigin()
    local normal = data:GetNormal()
    local flDamage = data:GetMagnitude()

    -- Use the new particle system
    local dir = Vector(normal) -- * RandomVector( -0.05f, 0.05f )
    local offset = origin + normal

    local vecAngles = dir:Angle()

    local pEffectName
    if flDamage > damage_impact_heavy:GetInt() then
          pEffectName = "blood_impact_heavy"
    elseif flDamage >= damage_impact_medium:GetInt() then
        pEffectName = "blood_impact_medium"
    elseif flDamage > 1 then
        pEffectName = "blood_impact_light"
    else
        pEffectName = "blood_impact_light_headshot"
    end

    if SERVER then
        swcs.SendParticle(pEffectName, offset, vecAngles, false, nil, nil, owner)
    end

    ParticleEffect(pEffectName, offset, vecAngles)
end

local mp_flinch_punch_scale = CreateConVar("mp_flinch_punch_scale", "3", {FCVAR_REPLICATED, FCVAR_CHEAT}, "Scalar for first person view punch when getting hit." )

function swcs.fx.TraceAttack(pEnt, dmg, vecDir, tr)
    if not (pEnt:IsPlayer() or pEnt:IsNPC() or pEnt:IsNextBot()) then
        return end

    local bShouldBleed = swcs.fx.ShouldShowBlood(pEnt:GetBloodColor())
    local bShouldSpark = false

    if SERVER and pEnt:GetInternalVariable("m_takedamage") ~= 2 then
        return
    end

    local flDamage = dmg:GetDamage()

    local punchAngle
    local flAng

    local hitByBullet = false
    local hitByGrenadeProjectile = false
    local bHeadShot = false

    local flBodyDamageScale = 1 --(GetTeamNumber() == TEAM_CT) ? mp_damage_scale_ct_body.Getlocal() : mp_damage_scale_t_body.GetFloat();
    local flHeadDamageScale = 1 --(GetTeamNumber() == TEAM_CT) ? mp_damage_scale_ct_head.GetFloat() : mp_damage_scale_t_head.GetFloat();

    if bit.band(dmg:GetDamageType(), DMG_SHOCK) ~= 0 then
        bShouldBleed = false
    elseif bit.band(dmg:GetDamageType(), DMG_BLAST) ~= 0 then
        if pEnt:IsPlayer() and pEnt:Armor() > 0 then
            bShouldBleed = false
        end

        if bShouldBleed and pEnt:IsPlayer() then
            -- punch view if we have no armor

            local punchAngle = Angle()
            local wep = pEnt:GetActiveWeapon()
            if wep:IsValid() and wep.IsSWCSWeapon then
                punchAngle:Set(wep:GetRawAimPunchAngle())
            else
                punchAngle:Set(pEnt:GetViewPunchAngles())
            end

            punchAngle.x = mp_flinch_punch_scale:GetFloat() * flDamage * -0.1;

            if punchAngle.x < mp_flinch_punch_scale:GetFloat() * -4 then
                punchAngle.x = mp_flinch_punch_scale:GetFloat() * -4
            end

            if wep:IsValid() and wep.IsSWCSWeapon then
                wep:SetRawAimPunchAngle(punchAngle)
            else
                pEnt:SetViewPunchAngles(punchAngle)
            end
        end
    else
        local wep = dmg:GetInflictor()

        if wep:IsValid() and wep.IsSWCSWeapon then
            hitByBullet = true -- NOTE: maybe wrong? fix if issues?
            --hitByGrenadeProjectile
        end

        if tr.HitGroup == HITGROUP_HEAD then
            if pEnt:IsPlayer() and pEnt:HasHelmet() and not hitByGrenadeProjectile then
                bShouldSpark = true
            end

            flDamage = flDamage * 4
            flDamage = flDamage * flHeadDamageScale

            if pEnt:IsPlayer() and not pEnt:HasHelmet() then
                -- do flinch stuff here lol
            end

            bHeadShot = true
        elseif tr.HitGroup == HITGROUP_CHEST then
            flDamage = flDamage * 1.0
            flDamage = flDamage * flBodyDamageScale

            if pEnt:IsPlayer() and pEnt:Armor() <= 0 then
                flAng = -0.1
            else
                flAng = -0.005
            end

            -- do flinch stuff here lol
        elseif tr.HitGroup == HITGROUP_STOMACH then
            flDamage = flDamage * 1.25
            flDamage = flDamage * flBodyDamageScale

            if pEnt:IsPlayer() and pEnt:Armor() <= 0 then
                flAng = -0.1
            else
                flAng = -0.005
            end

            -- do flinch stuff here lol
        end
    end

    if SERVER and bShouldBleed then
        swcs.fx.TraceBleed(flDamage, vecDir, tr, dmg:GetDamageType())

        local data = EffectData()
        data:SetOrigin(tr.HitPos)
        data:SetNormal(vecDir)
        if SERVER then
            data:SetEntIndex(tr.Entity:IsValid() and tr.Entity:EntIndex() or 0)
        else
            data:SetEntity(tr.Entity)
        end
        data:SetMagnitude(flDamage)

        -- reduce blood effect if target has armor
        if pEnt:IsPlayer() and pEnt:Armor() > 0 then
            data:SetMagnitude(data:GetMagnitude() * 0.5)
        end

        -- reduce blood effect if target is hit in the helmet
        if tr.HitGroup == HITGROUP_HEAD and bShouldSpark then
            data:SetMagnitude(1)
        end

        swcs.fx.BloodSpray(data, dmg:GetAttacker())
    end

    if SERVER and tr.HitGroup == HITGROUP_HEAD and bShouldSpark then -- they hit a helmet
        -- show metal spark effect
        local angle = tr.HitNormal:Angle()

        if dmg:GetAttacker():IsPlayer() then
            swcs.SendParticle("impact_helmet_headshot", tr.HitPos, angle, false, nil, nil, dmg:GetAttacker())
        end

        ParticleEffect("impact_helmet_headshot", tr.HitPos, angle)
    end
end

function swcs.fx.TraceBleed(flDamage, vecDir, tr, bitsDamageType)
    local ent = tr.Entity
    if ent:GetBloodColor() == DONT_BLEED or ent:GetBloodColor() == BLOOD_COLOR_MECH then
        return end

    if flDamage == 0 then
        return end

    if bit.band(bitsDamageType, bit.bor(DMG_CRUSH, DMG_BULLET, DMG_SLASH, DMG_BLAST, DMG_CLUB, DMG_AIRBOAT)) == 0 then
        return end

    -- make blood decal on the wall!
    local bloodTr = {}
    local vecTraceDir
    local flNoise = 0
    local cCount = 0

    if flDamage < 10 then
        flNoise = 0.1
        cCount = 1
    elseif flDamage < 25 then
        flNoise = 0.2
        cCount = 2
    else
        flNoise = 0.3
        cCount = 4
    end

    local flTraceDist = bit.band(bitsDamageType, DMG_AIRBOAT) ~= 0 and 384 or 172

    for i = 0, cCount do
        vecTraceDir = vecDir * -1 -- trace in the opposite direction the shot came from (the direction the shot is going)

        vecTraceDir.x = vecTraceDir.x + g_ursRandom:RandomFloat( -flNoise, flNoise )
        vecTraceDir.y = vecTraceDir.y + g_ursRandom:RandomFloat( -flNoise, flNoise )
        vecTraceDir.z = vecTraceDir.z + g_ursRandom:RandomFloat( -flNoise, flNoise )

        -- Don't bleed on grates.
        util.TraceLine({
            start = tr.HitPos,
            endpos = tr.HitPos + vecTraceDir * -flTraceDist,
            mask = bit.band(MASK_SOLID_BRUSHONLY, bit.bnot(CONTENTS_GRATE)),
            collisiongroup = COLLISION_GROUP_NONE,
            filter = {ent},
            output = bloodTr,
        })

        if bloodTr.Hit then
            swcs.fx.BloodDecalTrace( bloodTr, ent:GetBloodColor(), ent )
        end
    end
end

function swcs.fx.BloodDecalTrace(tr, bloodColor, srcEnt)
    if swcs.fx.ShouldShowBlood(bloodColor) then
        if bloodColor == BLOOD_COLOR_RED then
            util.Decal("Blood", tr.StartPos, tr.HitPos + tr.Normal, srcEnt)
        else
            util.Decal("YellowBlood", tr.StartPos, tr.HitPos + tr.Normal, srcEnt)
        end
    end
end

local violence_hblood = GetConVar("violence_hblood")
local violence_ablood = GetConVar("violence_ablood")
function swcs.fx.ShouldShowBlood(color)
    if color ~= DONT_BLEED then
        if color == BLOOD_COLOR_RED then
            return violence_hblood:GetBool()
        else
            return violence_ablood:GetBool()
        end
    end

    return false
end
