AddCSLuaFile()

local Approach = swcs.Approach
local Gain = swcs.Gain
local Bias = swcs.Bias
local IRONSIGHT_ANGLE_AVERAGE_SIZE = 8
local IRONSIGHT_ANGLE_AVERAGE_DIVIDE = 1 / IRONSIGHT_ANGLE_AVERAGE_SIZE

local IronSight_should_approach_unsighted = 0
local IronSight_should_approach_sighted = 1
local IronSight_viewmodel_is_deploying = 2
local IronSight_weapon_is_dropped = 3

local IRONSIGHT_HIDE_CROSSHAIR_THRESHOLD = 0.5

local IRONSIGHT_VIEWMODEL_BOB_MULT_X = 0.05
local IRONSIGHT_VIEWMODEL_BOB_PERIOD_X = 6
local IRONSIGHT_VIEWMODEL_BOB_MULT_Y = 0.1
local IRONSIGHT_VIEWMODEL_BOB_PERIOD_Y = 10

local META = {
    m_bIronSightAvailable			= false,
    m_angPivotAngle					= Angle(),
    m_vecEyePos						= Vector(),

    m_flIronSightAmount				= 0.0,
    m_flIronSightPullUpSpeed		= 8.0,
    m_flIronSightPutDownSpeed		= 4.0,
    m_flIronSightFOV				= 80.0,
    m_flIronSightPivotForward		= 10.0,
    m_flIronSightLooseness			= 0.5,
    m_pAttachedWeapon				= NULL,

    -- client only
    m_angDeltaAverage				= {},
    m_angViewLast					= Angle(),
    m_vecDotCoords					= Vector(),
    m_flDotBlur						= 0.0,
    m_flSpeedRatio					= 0.0,
}

swcs.DefineInterpolatedVar(META, "m_flIronSightAmount", "IronSightAmount")
META.m_flIronSightAmountLast = 0.0
META.m_flIronSightAmountUninterpolated = 0.0

swcs.DefineInterpolatedVar(META, "m_flSpeedRatio", "SpeedRatio")
META.m_flSpeedRatioLast = 0.0
META.m_flSpeedRatioUninterpolated = 0.0

for i = 0, IRONSIGHT_ANGLE_AVERAGE_SIZE do
    META.m_angDeltaAverage[i] = Angle()
end
META.__index = META
META.__tostring = function(self)
    return "IronSightController [" .. tostring(self.m_pAttachedWeapon) .. "]"
end

function META:IsApproachingSighted()
    return IsValid(self.m_pAttachedWeapon) and self.m_pAttachedWeapon:GetIronSightMode() == IronSight_should_approach_sighted
end
function META:IsApproachingUnSighted()
    return IsValid(self.m_pAttachedWeapon) and self.m_pAttachedWeapon:GetIronSightMode() == IronSight_should_approach_unsighted
end
function META:IsDeploying()
    return IsValid(self.m_pAttachedWeapon) and self.m_pAttachedWeapon:GetIronSightMode() == IronSight_viewmodel_is_deploying
end
function META:IsDropped()
    return IsValid(self.m_pAttachedWeapon) and self.m_pAttachedWeapon:GetIronSightMode() == IronSight_weapon_is_dropped
end

function META:UpdateIronSightAmount()
    if not IsValid(self.m_pAttachedWeapon) or self:IsDropped() or self:IsDeploying() then
        -- ignore and discard any lingering ironsight amount.
        self:SetIronSightAmount(0)
        self.m_flIronSightAmountGained = 0.0
        return
    end

    -- first determine if we are going into or out of ironsights, and set m_flIronSightAmount accordingly
    local flIronSightAmountTarget = self:IsApproachingSighted() and 1.0 or 0.0
    local flIronSightUpdOrDownSpeed = self:IsApproachingSighted() and self.m_flIronSightPullUpSpeed or self.m_flIronSightPutDownSpeed

    -- LUA: magic scalar by 0.5 bc it feels off when at 100% frametime speed
    if game.SinglePlayer() or IsFirstTimePredicted() then
        self:SetIronSightAmount(Approach(flIronSightAmountTarget, self:GetIronSightAmount(false), FrameTime() * flIronSightUpdOrDownSpeed))
    end

    self.m_flIronSightAmountGained = Gain( self:GetIronSightAmount(), 0.8 )
    self.m_flIronSightAmountBiased = Bias( self:GetIronSightAmount(), 0.2 )
end

function META:IsInIronSight()
    if IsValid(self.m_pAttachedWeapon) then
        if self:IsDeploying() or
            self:IsDropped() or
            self.m_pAttachedWeapon:GetInReload() or
            self.m_pAttachedWeapon:GetDoneSwitchingSilencer() >= CurTime() then
            return false
        end

        -- if looking at wep, return false

        if self:GetIronSightAmount() > 0 and (self:IsApproachingSighted() or self:IsApproachingUnSighted()) then
            return true
        end
    end

    return false
end

local function AngleDiff(destAngle, srcAngle)
    local delta = math.fmod(destAngle - srcAngle, 360)
    if destAngle > srcAngle then
        if delta >= 180 then
            delta = delta - 360
        end
    else
        if delta <= -180 then
            delta = delta + 360
        end
    end

    return delta
end

function META:QAngleDiff(angTarget, angSrc)
    return Angle(	AngleDiff(angTarget.x, angSrc.x),
                    AngleDiff(angTarget.y, angSrc.y),
                    AngleDiff(angTarget.z, angSrc.z) )
end

function META:AddToAngleAverage(newAngle)
    if self:GetIronSightAmount() < 1 then return end

    newAngle.x = math.Clamp(newAngle.x, -2, 2)
    newAngle.y = math.Clamp(newAngle.y, -2, 2)
    newAngle.z = math.Clamp(newAngle.z, -2, 2)

    -- essentially table.insert
    for i = IRONSIGHT_ANGLE_AVERAGE_SIZE, 0, -1 do
        self.m_angDeltaAverage[i] = self.m_angDeltaAverage[i - 1]
    end

    self.m_angDeltaAverage[0] = newAngle
end

function META:GetAngleAverage()
    local temp = Angle()

    if self:GetIronSightAmount() < 1 then return temp end

    for i = 0, IRONSIGHT_ANGLE_AVERAGE_SIZE do
        temp = temp + self.m_angDeltaAverage[i]
    end

    return temp * IRONSIGHT_ANGLE_AVERAGE_DIVIDE
end

local ironsight_catchupspeed = 60
local LAST_FRAME = 0
function META:ApplyIronSightPositioning(vecPosition, angAngle)
    self:UpdateIronSightAmount()

    if self:GetIronSightAmount() == 0 then return end

    if self.m_pAttachedWeapon:GetPlayerOwner() then
        local ply = self.m_pAttachedWeapon:GetPlayerOwner()
        self:SetSpeedRatio(Approach(
            ply:GetVelocity():Length() / self.m_pAttachedWeapon:GetMaxSpeed(),
            self:GetSpeedRatio(),
            -- LUA: magic scalar by 0.5 bc it feels off when at 100% frametime speed
            swcs.FrameTime() * 10)
        )
    end

    -- if we're more than 10% ironsighted, apply looseness.
    if self:GetIronSightAmount() > 0.1 then
        -- get the difference between current angles and last angles
        local angDelta = self:QAngleDiff(self.m_angViewLast, angAngle)

        -- dampen the delta to simulate 'looseness', but the faster we move, the more looseness approaches ironsight_running_looseness.GetFloat(), which is as waggly as possible
        if game.SinglePlayer() or IsFirstTimePredicted() then
            self:AddToAngleAverage(angDelta * Lerp(self:GetSpeedRatio(), self.m_flIronSightLooseness, .3))
        end

        -- m_angViewLast tries to catch up to angAngle
        self.m_angViewLast:Sub(angDelta * math.Clamp(swcs.FrameTime() * ironsight_catchupspeed, 0, 1))
    else
        self.m_angViewLast:Set(angAngle)
    end

    -- now the fun part - move the viewmodel to look down the sights

    -- create a working matrix at the current eye position and angles
    local matIronSightMatrix = Matrix()
    matIronSightMatrix:Translate(vecPosition)
    matIronSightMatrix:SetAngles(angAngle)

    -- offset the matrix by the ironsight eye position
    matIronSightMatrix:Translate((-self.m_vecEyePos) * self.m_flIronSightAmountGained)

    -- additionally offset by the ironsight origin of rotation, the weapon will pivot around this offset from the eye
    matIronSightMatrix:Translate(Vector(self.m_flIronSightPivotForward, 0, 0))

    local angDeltaAverage = self:GetAngleAverage()

    -- apply ironsight eye rotation
    -- use schema defined angles
    local temp = Angle()
    temp:RotateAroundAxis(Vector(1, 0, 0), (angDeltaAverage.z + self.m_angPivotAngle.z) * self.m_flIronSightAmountGained)
    temp:RotateAroundAxis(Vector(0, 1, 0), (angDeltaAverage.x + self.m_angPivotAngle.x) * self.m_flIronSightAmountGained)
    temp:RotateAroundAxis(Vector(0, 0, 1), (angDeltaAverage.y + self.m_angPivotAngle.y) * self.m_flIronSightAmountGained)
    matIronSightMatrix:Rotate(temp)

    -- move the weapon back to the ironsight eye position
    matIronSightMatrix:Translate(Vector(-self.m_flIronSightPivotForward, 0, 0))

    -- if the player is moving, pull down and re-bob the weapon
    if self.m_pAttachedWeapon:GetPlayerOwner() then
        local vecIronSightBob = Vector(
            1,
            IRONSIGHT_VIEWMODEL_BOB_MULT_X * math.sin(CurTime() * IRONSIGHT_VIEWMODEL_BOB_PERIOD_X),
            IRONSIGHT_VIEWMODEL_BOB_MULT_Y * math.sin(CurTime() * IRONSIGHT_VIEWMODEL_BOB_PERIOD_Y) - IRONSIGHT_VIEWMODEL_BOB_MULT_Y
        )

        self.m_vecDotCoords.x = -vecIronSightBob.y
        self.m_vecDotCoords.y = -vecIronSightBob.z
        self.m_vecDotCoords:Mul(0.1)
        self.m_vecDotCoords.x = self.m_vecDotCoords.x * 0.6
        self.m_vecDotCoords.x = self.m_vecDotCoords.x - angDeltaAverage.y * 0.03

        self.m_vecDotCoords.y = self.m_vecDotCoords.y * 0.2
        self.m_vecDotCoords.y = self.m_vecDotCoords.y + angDeltaAverage.x * 0.03
        self.m_vecDotCoords:Mul(self:GetSpeedRatio())

        --[[
            if ( !cl_righthand.GetBool() )
                vecIronSightBob.y = -vecIronSightBob.y;
        ]]

        matIronSightMatrix:Translate(vecIronSightBob * self:GetSpeedRatio())
    end

    -- extract the final position and angles and apply them as differences from the passed in values
    vecPosition:Sub(vecPosition - matIronSightMatrix:GetTranslation())

    local angIronSightAngles = matIronSightMatrix:GetAngles()
    angAngle:Sub(self:QAngleDiff(angAngle, angIronSightAngles))

    --dampen dot blur
    if CLIENT and LAST_FRAME ~= FrameNumber() then
        LAST_FRAME = FrameNumber()
        self.m_flDotBlur = Approach(0, self.m_flDotBlur, swcs.FrameTime())
    end
end

function META:SetState(newState)
    if newState == IronSight_viewmodel_is_deploying or newState == IronSight_weapon_is_dropped then
        self:SetIronSightAmount(0)
    end
    if self.m_pAttachedWeapon:IsValid() and self.m_pAttachedWeapon.IsSWCSWeapon then
        self.m_pAttachedWeapon:SetIronSightMode(newState)
    end
end

function META:IsInitializedAndAvailable()
    return self.m_bIronSightAvailable
end

function META:Init(wep)
    if self:IsInitializedAndAvailable() then return true end

    if wep:IsValid() and wep:IsWeapon() then
        if wep.IsAug then
            self.m_pAttachedWeapon = wep

            self.m_bIronSightAvailable			= true
            self.m_flIronSightLooseness			= 0.03
            self.m_flIronSightPullUpSpeed		= 10.0
            self.m_flIronSightPutDownSpeed		= 8.0
            self.m_flIronSightFOV				= 45.0
            self.m_flIronSightPivotForward		= 10.0
            self.m_vecEyePos						= Vector( -1.56, -3.6, -0.07 )
            self.m_angPivotAngle					= Angle( 0.78, -0.1, -0.03 )

            return true
        elseif wep.IsSg556 then
            self.m_pAttachedWeapon = wep

            self.m_bIronSightAvailable			= true
            self.m_flIronSightLooseness			= 0.03
            self.m_flIronSightPullUpSpeed		= 10.0
            self.m_flIronSightPutDownSpeed		= 8.0
            self.m_flIronSightFOV				= 45.0
            self.m_flIronSightPivotForward		= 8.0
            self.m_vecEyePos						= Vector(0.72, -5.12, -1.33)
            self.m_angPivotAngle					= Angle(0.52, 0.04, 0.72)

            return true
        end

        if wep.ItemAttributes and tobool(wep.ItemAttributes["aimsight capable"]) then
            self.m_bIronSightAvailable = true
            self.m_pAttachedWeapon = wep

            self.m_flIronSightLooseness = tonumber(wep.ItemAttributes["aimsight looseness"])
            self.m_flIronSightPullUpSpeed = tonumber(wep.ItemAttributes["aimsight speed up"])
            self.m_flIronSightPutDownSpeed = tonumber(wep.ItemAttributes["aimsight speed down"])
            self.m_flIronSightFOV = tonumber(wep.ItemAttributes["aimsight fov"])
            self.m_flIronSightPivotForward = tonumber(wep.ItemAttributes["aimsight pivot forward"])
            self.m_vecEyePos = Vector(wep.ItemAttributes["aimsight eye pos"])
            self.m_angPivotAngle = Angle(wep.ItemAttributes["aimsight pivot angle"])

            return true
        end
    end

    return false
end

function META:ShouldHideCrossHair()
    return (self:IsApproachingSighted() or self:IsApproachingUnSighted()) and self:GetIronSightAmount() > IRONSIGHT_HIDE_CROSSHAIR_THRESHOLD
end

function META:GetDotMaterial()
    if self.m_pAttachedWeapon:IsValid() and self.m_pAttachedWeapon.IsSWCSWeapon then
        if self.m_pAttachedWeapon.IsAug then
            return "models/weapons/shared/scope/scope_dot_green"
        elseif self.m_pAttachedWeapon.IsSg556 then
            return "models/weapons/shared/scope/scope_dot_red"
        else
            return self.m_pAttachedWeapon.ItemAttributes["aimsight material"] == ""
                and "null"
                or (self.m_pAttachedWeapon.ItemAttributes["aimsight material"] or "models/weapons/shared/scope/scope_dot_green")
        end
    end
end
function META:IncreaseDotBlur(flAmount)
    self.m_flDotBlur = math.Clamp(self.m_flDotBlur + flAmount, 0, 1)
end
function META:GetDotBlur()
    return Bias(1 - math.max(self.m_flDotBlur, self:GetSpeedRatio() * 0.5), 0.2)
end
function META:GetDotWidth()
    return 32 + (256 * math.max(self.m_flDotBlur, self:GetSpeedRatio() * 0.3))
end

function META:GetIronSightFOV(flDefaultFOV, bUseBiasedValue)
    -- sets biased value between the current FOV and the ideal IronSight FOV based on how 'ironsighted' the weapon currently is
    if not self:IsInIronSight() then
        return flDefaultFOV end

    local flIronSightFOVAmount = bUseBiasedValue and self.m_flIronSightAmountBiased or self:GetIronSightAmount()

    return Lerp(flIronSightFOVAmount, flDefaultFOV, self.m_flIronSightFOV)
end

local s_RatioToAspectModes = {
    { 0, 4.0 / 3.0 },
    { 1, 16.0 / 9.0 },
    { 2, 16.0 / 10.0 },
    { 2, 1.0 },
}
local function GetScreenAspectRatio(width, height)
    local aspectRatio = width / height

    -- just find the closest ratio
    local closestAspectRatioDist = 99999.0
    local closestAnamorphic = 0
    for i = 1, #s_RatioToAspectModes do
        local dist = math.abs( s_RatioToAspectModes[i][2] - aspectRatio )
        if (dist < closestAspectRatioDist) then
            closestAspectRatioDist = dist
            closestAnamorphic = s_RatioToAspectModes[i][1]
        end
    end

    return closestAnamorphic
end
local rt_vm
if CLIENT then
    rt_vm = GetRenderTarget("swcs_rt_vm", 2048, 2048)
end

function META:PrepareScopeEffect(x, y, w, h)
    if not self:IsInIronSight() then return false end

    render.PushRenderTarget(rt_vm)
        render.BlurRenderTarget(rt_vm, 2.5, 2.5, 1)
    render.PopRenderTarget()

    -- Prepare to render the scope lens mask shape into the stencil buffer.
    -- The weapon itself will take care of using the correct blend mode and override material.
    render.SetStencilEnable(true)
    render.SetStencilWriteMask(0xFF)
    render.SetStencilTestMask(0xFF)
    render.SetStencilReferenceValue(1)
    render.SetStencilCompareFunction(STENCIL_NOTEQUAL)
    render.SetStencilPassOperation(STENCIL_REPLACE)

    return true
end

function META:RenderScopeEffect(x, y, w, h)
    if not self:IsInIronSight() then return end

    -- apply the blur effect to the screen while masking out the scope lens
    -- stencilState_skip_scope_lens_pixels
    render.SetStencilEnable(true)
    render.SetStencilReferenceValue(1)
    render.SetStencilCompareFunction(STENCIL_NOTEQUAL)
    render.SetStencilPassOperation(STENCIL_KEEP)
    render.SetStencilFailOperation(STENCIL_KEEP)
    render.SetStencilZFailOperation(STENCIL_KEEP)

    -- RENDER _rt_SmallFB0 to screen
    local pBlurOverlayMaterial = Material("dev/scope_bluroverlay")

    -- set alpha to the amount of ironsightedness
    local flAlphaVar = pBlurOverlayMaterial:GetFloat("$alpha")
    if flAlphaVar then
        pBlurOverlayMaterial:SetFloat("$alpha", Bias( self:GetIronSightAmount(), 0.2))
    end

    -- render background blur & scope dot
    cam.Start2D()
        render.OverrideDepthEnable(true, false)

        render.SetMaterial(pBlurOverlayMaterial)
        render.DrawScreenQuad()

        -- now draw the laser dot, masked to ONLY render on the lens
        local dotCoords = self.m_vecDotCoords
        dotCoords.x = dotCoords.x * GetScreenAspectRatio(w, h)

        -- stencilState_use_only_scope_lens_pixels
        render.SetStencilEnable(true)
        render.SetStencilReferenceValue(1)
        render.SetStencilCompareFunction(STENCIL_EQUAL)
        render.SetStencilPassOperation(STENCIL_KEEP)
        render.SetStencilFailOperation(STENCIL_KEEP)
        render.SetStencilZFailOperation(STENCIL_REPLACE)

        local iWidth = self:GetDotWidth()

        local pMatDot = Material(self:GetDotMaterial())
        local alphaVar2 = pMatDot:GetFloat("$alpha")
        if alphaVar2 then
            local flDotBlur = self:GetDotBlur()
            pMatDot:SetFloat("$alpha", flDotBlur == flDotBlur and flDotBlur or 0)
        end

        dotCoords.x = (dotCoords.x * 2) + 0.5
        dotCoords.y = (dotCoords.y * 3) + 0.5

        render.SetMaterial(pMatDot)
        render.SetBlend(self:GetDotBlur())
        render.DrawScreenQuadEx(
            (w * dotCoords.x) - (iWidth / 2),
            (h * dotCoords.y) - (iWidth / 2),
            iWidth, iWidth)
    cam.End2D()

    render.OverrideDepthEnable(false, true)

    -- restore a disabled stencil state
    render.SetStencilEnable(false)

    --clean up stencil buffer once we're done so render elements like the glow pass draw correctly
    render.ClearStencil()
end

-- pull up duration is how long the pull up would take in seconds, not the speed
function META:GetIronSightPullUpDuration()
    return self.m_flIronSightPullUpSpeed > 0 and (1 / self.m_flIronSightPullUpSpeed) or 0
end
function META:GetIronSightPutDownDuration()
    return self.m_flIronSightPutDownSpeed > 0 and (1 / self.m_flIronSightPutDownSpeed) or 0
end

function IronSightController(t)
    return setmetatable(istable(t) and t or {}, META)
end

function IsIronSightController(o)
    return getmetatable(o) == META
end
