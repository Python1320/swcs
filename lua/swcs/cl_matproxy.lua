-- material proxies
do
    matproxy.Add({
        name = "IronSightAmount",
        init = function(self, mat, values)
            self.m_bInvert = tobool(values.invert or 0)
            self.m_strResultVar = values.resultvar
        end,
        bind = function(self, mat, ent)
            if not ent:IsValid() then return end
            local owner = ent:GetOwner()

            -- wtf
            if not IsValid(owner) then return end

            -- wtf!!!!
            local wep = owner:GetActiveWeapon()
            if not wep:IsValid() then return end

            local iron = wep.m_IronSightController
            if iron then
                local var = iron:GetIronSightAmount()

                if self.m_bInvert then
                    var = 1 - var
                end

                mat:SetFloat(self.m_strResultVar, var)
            end
        end
    })

    local cl_crosshaircolor = GetConVar"cl_crosshaircolor"
    local cl_crosshaircolor_r = GetConVar"cl_crosshaircolor_r"
    local cl_crosshaircolor_g = GetConVar"cl_crosshaircolor_g"
    local cl_crosshaircolor_b = GetConVar"cl_crosshaircolor_b"

    local SWITCH_CrosshairColor = {
        [0] = Color(250, 50, 50),
        Color(50, 250, 50),
        Color(250, 250, 50),
        Color(50, 50, 250),
        Color(50, 250, 250),
        function ()
            return Color(
                cl_crosshaircolor_r:GetInt(),
                cl_crosshaircolor_g:GetInt(),
                cl_crosshaircolor_b:GetInt()
            )
        end,
    }

    matproxy.Add({
        name = "CrossHairColor",
        init = function(self, mat, values)
            self.m_strResultVar = values.resultvar
        end,
        bind = function(self, mat, ent)
            local col
            if SWITCH_CrosshairColor[cl_crosshaircolor:GetInt()] then
                local _col = SWITCH_CrosshairColor[cl_crosshaircolor:GetInt()]

                if isfunction(_col) then
                    _col = _col()
                end

                col = _col
            end

            math.Remap(col.r, 0, 255, 0, 3)
            mat:SetVector(self.m_strResultVar, Vector(
                math.Remap(col.r, 0, 255, 0, 3),
                math.Remap(col.g, 0, 255, 0, 3),
                math.Remap(col.b, 0, 255, 0, 3))
            )
        end
    })

    -- in all valve games since l4d1, but not gmod
    matproxy.Add({
        name = "ConVar",
        init = function(self, mat, values)
            self.m_strResultVar = values.resultvar
            self.m_strConVar = values.convar
        end,
        bind = function(self, mat, ent)
            if not self.m_ConVar then
                self.m_ConVar = GetConVar(self.m_strConVar)
                --mat:SetInt(self.m_strResultVar, 1)
                return
            end

            mat:SetInt(self.m_strResultVar, self.m_ConVar:GetInt())
        end
    })

    matproxy.Add({
        name = "Equals",
        init = function(self, mat, values)
            self.m_strResultVar = values.resultvar
            self.m_strSrcVar1 = values.srcvar1
        end,
        bind = function(self, mat, ent)
            local result = mat:GetFloat(self.m_strResultVar)

            -- naively assume it's a vector
            if not result then
                local _,_, index = self.m_strResultVar:find("%[(%d+)%]")
                index = tonumber(index)
                if not isnumber(index) then return end

                local baseResult = self.m_strResultVar:gsub("(%[%d+%])", "")
                result = mat:GetVector(baseResult)

                if result then
                    result[index] = mat:GetFloat(self.m_strSrcVar1)
                    mat:SetVector(baseResult, result)
                end
                --print(self.m_strResultVar, baseResult, index, result[index + 1])
            end

            --print(self.m_strResultVar, result)
            --local val = mat:GetFloat(self.m_strSrcVar1)
            --local result = mat:GetFloat(self.m_strResultVar:gsub("(%[%d+%])", ""))


            --print(self.m_strSrcVar1, val, self.m_strResultVar:gsub("[%d+]", ""), result)
            --mat:SetInt(self.m_strResultVar, self.m_ConVar:GetInt())
        end
    })

    --[[
        matproxy.Add({
            name = "Multiply",
            init = function(self, mat, values)
                self.m_strSrcVar1 = values.srcvar1
                self.m_strSrcVar2 = values.srcvar2
                self.m_strResultVar = values.resultvar
            end,
            bind = function(self, mat, ent)
                local src1, src2 = mat:GetFloat(self.m_strSrcVar1), mat:GetFloat(self.m_strSrcVar2)
                local val = src1 * src2
                mat:SetFloat(self.m_strResultVar, val)
            end
        })

        matproxy.Add({
            name = "Clamp",
            init = function(self, mat, values)
                self.m_strSrcVar1 = values.srcvar1
                self.m_strResultVar = values.resultvar
                self.m_strMin = values.min
                self.m_strMax = values.max
            end,
            bind = function(self, mat, ent)
                local val = mat:GetFloat(self.m_strSrcVar1)
                local fMin, fMax

                if tonumber(self.m_strMin) then
                    fMin = tonumber(self.m_strMin)
                else
                    fMin = mat:GetFloat(self.m_strMin)
                end

                if tonumber(self.m_strMax) then
                    fMax = tonumber(self.m_strMax)
                else
                    fMax = mat:GetFloat(self.m_strMax)
                end

                val = math.Clamp(val, fMin, fMax)
                mat:SetFloat(self.m_strResultVar, val)
            end
        })
    ]]
end