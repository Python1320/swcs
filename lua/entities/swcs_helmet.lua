AddCSLuaFile()
ENT.Base = "swcs_base_item"
ENT.Category = "CS:GO"
ENT.Spawnable = true
ENT.Model = Model("models/weapons/csgo/w_eq_helmet.mdl")

function ENT:CanInteract(actor)
    if actor:HasHelmet() then
        return false
    end

    return true
end

function ENT:OnInteract(actor)
    actor:GiveHelmet()
end

