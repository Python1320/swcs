AddCSLuaFile()

ENT.Base = "baseswcsgrenade_projectile"
ENT.m_flTimeToDetonate = 1.5


DEFINE_BASECLASS(ENT.Base)

local TAG = "swcs_sensorgrenade_projectile"

local GRENADE_MODEL = "models/weapons/csgo/w_eq_sensorgrenade_thrown.mdl"

local THINK_ARM = 1
local THINK_SENSOR = 2
local THINK_REMOVE = 3

ENT.ThinkFuncs = {
    [THINK_ARM] = function(self)
        if self:GetFinalVelocity():Length() > 0.2 then
            self:NextThink(0.2)
            return true
        end

        self:SetExpireTime(CurTime() + 2.0)
        self:SetThinkFuncIndex(THINK_SENSOR)
        self:CallThinkFunc(THINK_SENSOR)
        return true
    end,
    [THINK_SENSOR] = function(self)
        local pThrower = self:GetThrower()
        if not pThrower:IsValid() then
            return
        end

        if CurTime() > self:GetNextDetectPlayerSound() then
            self:EmitSound( "Sensor.WarmupBeep" )
            self:SetNextDetectPlayerSound(CurTime() + 1.0)
        end

        if CurTime() < self:GetExpireTime() then
            self:NextThink(CurTime() + 0.1)
            return true
        else
            if CLIENT then
                -- particle weapon_sensorgren_detonate PATTACH_POINT self "Wick"
                CreateParticleSystem(self, "weapon_sensorgren_detonate", PATTACH_POINT, self:LookupAttachment("Wick"))
            end

            if SERVER or (CLIENT and IsFirstTimePredicted()) then
                self:EmitSound( "Sensor.Detonate" )
            end

            self:DoDetectWave()
        end

        return true
    end,
    [THINK_REMOVE] = function(self)
        --if SERVER then self:Remove() end
        return true
    end,
}

local flMaxTraceDist = 1600

function ENT:DoDetectWave()
    -- tell the bots about the gunfire
    local hThrower = self:GetThrower()
    if not IsValid(hThrower) then
        return
    end

    local tTargettedPlayers = {}
    for _, ply in ipairs(ents.FindInSphere(self:GetPos(), flMaxTraceDist)) do
        if not ply:IsPlayer() then continue end
        if ply == hThrower then continue end

        local flDistance = ply:EyePos():Distance(self:GetPos())
        --[[
            if IsLineBlockedBySmoke(ply:EyePos(), self:GetPos(), 1) then
                -- if we are outside half the max dist and don't trace, dont show
                if flDistance > flMaxTraceDist / 2 then
                    continue
                end
            end
        ]]

        local tr = util.TraceLine({
            start = ply:EyePos(),
            endpos = self:GetPos(),
            mask = MASK_VISIBLE,
            filter = ply,
            collisiongroup = COLLISION_GROUP_DEBRIS,
        })

        if tr.Hit then
            local tr2 = util.TraceLine({
                start = ply:GetPos() + Vector(0, 0, 16),
                endpos = self:GetPos(),
                mask = MASK_VISIBLE,
                filter = ply,
                collisiongroup = COLLISION_GROUP_DEBRIS,
            })

            if tr2.Hit then
                -- if we are outside half the max dist and don't trace, dont show
                if flDistance > flMaxTraceDist / 2 then
                    continue
                end
            end
        end

        table.insert(tTargettedPlayers, ply)
    end

    if SERVER then
        hook.Run("SensorGrenadeDetonate", self, tTargettedPlayers)
    end

    --self:NextThink(0.25)
    --self:SetThinkFuncIndex(THINK_REMOVE)
    self:SetThinkFuncIndex(THINK_REMOVE)
    if SERVER then SafeRemoveEntityDelayed(self, 0.25) end
end

if SERVER then
    hook.Add("SensorGrenadeDetonate", "swcs.tanade", function(grenade, tPlayers)
        if not grenade:GetThrower():IsValid() then return end

        net.Start(TAG)
            --net.WriteEntity(grenade)
            net.WriteUInt(#tPlayers, 8)

            for _,ply in ipairs(tPlayers) do
                net.WriteEntity(ply)

                --ply:SetIsSpotted(true)
                --ply:SetIsSpottedBy(grenade:GetThrower())
                --ply.m_flDetectedByEnemySensorTime = CurTime()
                ply:Blind(0.2, 1.0, 128)
                ply:EmitSound("Sensor.WarmupBeep")
            end

        net.Send(grenade:GetThrower())
    end)
else
    local tWallhackedPlayers = {}
    net.Receive(TAG, function()
        --local grenade = net.ReadEntity()
        local count = net.ReadUInt(8)

        for i = 1, count do
            local ply = net.ReadEntity()
            if not ply:IsValid() then continue end

            table.insert(tWallhackedPlayers, {
                ply = ply,
                time = CurTime() + 5,
            })
        end
    end)

    hook.Add("PreDrawHalos", "swcs.tanade", function()
        if #tWallhackedPlayers == 0 then return end

        local t = {}

        local i = 1
        repeat
            local v = tWallhackedPlayers[i]
            if not v.ply:IsValid() or not v.ply:Alive() or v.time < CurTime() then
                table.remove(tWallhackedPlayers, i)
                continue
            end

            table.insert(t, v.ply)
            i = i + 1
        until i > #tWallhackedPlayers

        if #t > 0 then
            halo.Add(t, Color(255, 0, 0, 255), 1, 1, 1, true, true)
        end
    end)
end

function ENT:SetupDataTables()
    BaseClass.SetupDataTables(self)

    self:NetworkVar("Int", 3, "ThinkFuncIndex")
    self:NetworkVar("Float", 1, "ExpireTime")
    self:NetworkVar("Float", 2, "NextDetectPlayerSound")
end

function ENT:Create(pos, angs, vel, angvel, owner)
    self:SetPos(pos)
    self:SetAngles(angs)

    self:SetVelocity(vel)
    self:SetInitialVelocity(vel)

    if IsValid(owner) then
        self:SetThrower(owner)
        self:SetOwner(owner)
    end

    self:SetTimer(2.0)
    self:SetTimeToDetonate(CurTime() + 9999)

    self:SetLocalAngularVelocity(angvel)
    self:SetFinalAngularVelocity(angvel)
    self:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)

    return self
end

function ENT:SetTimer(flTimer)
    self:SetThinkFuncIndex(THINK_ARM)

    self:NextThink(flTimer)
    self:SetNextDetectPlayerSound(CurTime())
end

function ENT:Initialize()
    self:SetModel(GRENADE_MODEL)

    self:SetDetonateTimerLength( self.m_flTimeToDetonate )

    BaseClass.Initialize(self)
end

function ENT:Detonate()
    assert(false, "SensorGrenade grenade handles its own detonation")
end

function ENT:BounceSound()
    self:EmitSound("Flashbang.Bounce")
end

function ENT:OnBounced(tr, other)
    if bit.band(other:GetSolidFlags(), bit.bor(FSOLID_TRIGGER, FSOLID_VOLUME_CONTENTS)) ~= 0 then
        return
    end

    -- don't hit the guy that launched this grenade
    if other == self:GetThrower() then
        return
    end

    if other:GetClass() == "func_breakable" then
        return
    end

    if other:GetClass() == "func_breakable_surf" then
        return
    end

    -- don't detonate on ladders
    if other:GetClass() == "func_ladder" then
        return
    end

    if other:IsNPC() or other:IsPlayer() or other:IsNextBot() then
        -- don't break if we hit an actor - wait until we hit the environment
        return
    else
        self:SetNWMoveType(MOVETYPE_NONE)
        self:SetAbsVelocity(vector_origin)
        self:SetFinalVelocity(vector_origin)
        self:NextThink(1.0)
        self.AddtionalThink = self.ThinkArm

        self:EmitSound("Sensor.Activate")

        self:SetExpireTime(CurTime() + 15)

        -- stick the grenade onto the target surface using the closest rotational alignment to match the in-flight orientation,
        -- ( like breach charges )

        local vecSurfNormal = tr.HitNormal
        local vecProjectileZ = self:GetAngles():Forward()

        -- sensor grenades can stick on either of two sides, unlike the breach charges. So they don't need to flip when they land on their 'backs'.
        if vecSurfNormal:Dot(vecProjectileZ) < 0 then
            vecSurfNormal:Mul(-1)
        end

        local vecC4Right = vecProjectileZ:Cross(vecSurfNormal)
        local vecC4Forward = vecC4Right:Cross(-vecSurfNormal)
        local angSurface = vecC4Forward:AngleEx(vecSurfNormal)

        self:SetAngles(angSurface)
    end
end

function ENT:AdditionalThink()
    return self:CallThinkFunc(self:GetThinkFuncIndex())
end

function ENT:CallThinkFunc(iThinkFunc)
    if iThinkFunc ~= 0 then
        local fnThink = self.ThinkFuncs[iThinkFunc]
        if isfunction(fnThink) then
            return fnThink(self)
        end
    end
end
