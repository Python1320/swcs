AddCSLuaFile()
ENT.Base = "swcs_base_item"
ENT.Category = "CS:GO"
ENT.Spawnable = true
ENT.Model = Model("models/weapons/csgo/w_defuser.mdl")

function ENT:CanInteract(actor)
    return not actor:HasDefuser()
end

function ENT:OnInteract(actor)
    actor:GiveDefuser()
end