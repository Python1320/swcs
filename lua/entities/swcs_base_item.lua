AddCSLuaFile()
ENT.Type = "anim"
ENT.Spawnable = false
ENT.Category = "CS:GO"
ENT.Model = Model("models/weapons/csgo/v_shield.mdl")

function ENT:Initialize()
    self:SetModel(self.Model)

    if SERVER then
        self:PhysicsInit(SOLID_VPHYSICS)
        self:SetTrigger(true)

        local phys = self:GetPhysicsObject()

        if not phys:IsValid() then
            local mins, maxs = self:GetModelBounds()
            self:PhysicsInitBox(mins, maxs)
        end

        phys = self:GetPhysicsObject()
        if phys:IsValid() then
            --phys:EnableMotion(true)
            phys:Wake()
        end

        self:SetUseType(SIMPLE_USE)
    end

    self:SetCollisionGroup(COLLISION_GROUP_WEAPON)
end

function ENT:OnInteract(actor)
    --
end
function ENT:CanInteract(actor)
    return true
end

function ENT:Use(actor,caller)
    if not (actor:IsValid() and actor:IsPlayer()) then
        return end
    if not self:CanInteract(actor) then
        return end

    self:OnInteract(actor)

    -- better not use any fucking timers in OnInteract
    self:Remove()
end

function ENT:StartTouch(ent)
    if not (ent:IsValid() and ent:IsPlayer()) then
        return end
    if not self:CanInteract(ent) then
        return end

    self:OnInteract(ent)

    self:Remove()
    --SafeRemoveEntityDelayed(self, 0)
end
