AddCSLuaFile()
ENT.Base = "swcs_base_item"
ENT.Category = "CS:GO"
ENT.Spawnable = true
ENT.Model = Model("models/weapons/csgo/w_eq_armor.mdl")

function ENT:CanInteract(actor)
    if actor:Armor() >= 100 then return false end

    return true
end

function ENT:OnInteract(actor)
    actor:SetArmor(100)
end
