AddCSLuaFile()

ENT.Type = "anim"
ENT.RenderGroup = RENDERGROUP_OPAQUE

ENT.m_tTouchTrace = {}

SWCS_GRENADE_DEFAULT_SIZE = 2.0
SWCS_GRENADE_FAILSAFE_MAX_BOUNCES = 20

local CONTENTS_GRENADECLIP = 0x80000

ENT.IsSWCSGrenade = true
ENT.m_hOriginalThrower = NULL

function ENT:GetGrenadeGravity() return 0.4 end
function ENT:GetGrenadeFriction() return 0.2 end
function ENT:GetGrenadeElasticity() return 0.45 end

function ENT:SetupDataTables()
    self:NetworkVar("Vector", 0, "InitialVelocity")
    self:NetworkVar("Vector", 1, "_Pos")
    self:NetworkVar("Vector", 2, "FinalVelocity")
    self:NetworkVar("Angle", 0, "FinalAngularVelocity")
    self:NetworkVar("Int", 0, "Bounces")
    self:NetworkVar("Int", 1, "NWMoveType")
    self:NetworkVar("Int", 2, "ActualCollisionGroup")
    self:NetworkVar("Float", 0, "TimeToDetonate")
    self:NetworkVar("Entity", 0, "NWThrower")
end

function ENT:GetThrower()
    local hThrower = self:GetNWThrower()

    if not hThrower:IsValid() and self:GetOwner():IsValid() then
        return self:GetOwner()
    end

    return hThrower
end

function ENT:SetThrower(hThrower)
    self:SetNWThrower(hThrower)

    if not self.m_hOriginalThrower:IsValid() then
        self.m_hOriginalThrower = hThrower
    end
end

function ENT:Initialize()
    self:SetSolidFlags(FSOLID_NOT_STANDABLE)
    self:PhysicsInitBox(-Vector(1,1,1), Vector(1,1,1))
    self:SetMoveType(MOVETYPE_FLYGRAVITY)
    self:SetMoveCollide(MOVECOLLIDE_FLY_CUSTOM)
    self:SetSolidFlags(SOLID_BBOX) -- So it will collide with physics props!
    self:AddFlags(FL_GRENADE)

    if SERVER then
        self.m_LastHitPlayer = NULL
    end

    -- smaller, cube bounding box so we rest on the ground
    local min = Vector( -SWCS_GRENADE_DEFAULT_SIZE, -SWCS_GRENADE_DEFAULT_SIZE, -SWCS_GRENADE_DEFAULT_SIZE )
    local max = Vector( SWCS_GRENADE_DEFAULT_SIZE, SWCS_GRENADE_DEFAULT_SIZE, SWCS_GRENADE_DEFAULT_SIZE )

    self:SetCollisionBounds(min, max)
    self:SetBounces(0)

    local phys = self:GetPhysicsObject()
    if phys:IsValid() then
        phys:SetMass(0.4) -- 0.4 kg == 0.88 lbs
        phys:Wake()
    end

    self:Set_Pos(self:GetPos())
    self:SetFinalVelocity(self:GetInitialVelocity())
    self:SetNWMoveType(self:GetMoveType())

    self:SetActualCollisionGroup(self:GetCollisionGroup())
    self:SetCollisionGroup(COLLISION_GROUP_WEAPON)

    self:StartMotionController()

    self:SetGravity(self:GetGrenadeGravity())
    self:SetFriction(self:GetGrenadeFriction())
    self:SetElasticity(self:GetGrenadeElasticity())
end

function ENT:BounceSound() end
function ENT:OnBounced() end

-- Sets the time at which the grenade will explode
function ENT:SetDetonateTimerLength( flTimer )
    self:SetTimeToDetonate(CurTime() + flTimer)
end

function ENT:DetonateOnNextThink()
    self:SetDetonateTimerLength(0)
end

function ENT:Detonate()
    assert(false, "baseswcsgrenade_projectile:Detonate() should not be called. Make sure to implement this in your subclass!\n")
end

function ENT:Think()

    -- HACK fix for angular velocity being reset on bouncing
    local angularVel = self:GetLocalAngularVelocity()
    if angularVel:IsZero() then
        self:SetLocalAngularVelocity(self:GetFinalAngularVelocity())
    end

    self:SetMoveType(self:GetNWMoveType())

    self:SetPos(self:Get_Pos())
    if SERVER and not self:IsInWorld() then
        self:Remove()
        return
    end

    self:NextThink( 0.2 )

    if SERVER then
        sound.EmitHint(SOUND_DANGER, self:GetPos() + self:GetFinalVelocity() * 0.5, self:GetFinalVelocity():Length(), 0.2)
    end

    if self:WaterLevel() ~= 0 then
        self:SetFinalVelocity(self:GetFinalVelocity() * 0.5)
    end

    if self:AdditionalThink() then
        return true
    end

    if CurTime() > self:GetTimeToDetonate() then
        self:Detonate()
        return true
    end

    return true
end

function ENT:AdditionalThink() end

--[[
function ENT:UpdateTransmitState()
    -- always call ShouldTransmit() for grenades
    return self:ShouldTransmit()
end
]]

function ENT:ShouldTransmit()
    --[[CBaseEntity *pRecipientEntity = CBaseEntity::Instance( pInfo->m_pClientEnt );
    if ( pRecipientEntity->IsPlayer() )
    {
        CBasePlayer *pRecipientPlayer = static_cast<CBasePlayer*>( pRecipientEntity );

        // always transmit to the thrower of the grenade
        if ( pRecipientPlayer && ( (GetThrower() && pRecipientPlayer == GetThrower()) ||
            pRecipientPlayer->GetTeamNumber() == TEAM_SPECTATOR) )
        {
            return FL_EDICT_ALWAYS;
        }
    }

    return FL_EDICT_PVSCHECK;]]
end

function ENT:PhysicsSimulate(phys, dTime)
    if self:GetMoveType() == MOVETYPE_FLYGRAVITY then
        self:PhysicsToss(phys, dTime)
    end

    phys:UpdateShadow(self:GetPos(), Angle(), dTime)
    phys:SetPos(self:GetPos())
    phys:SetVelocity(self:GetFinalVelocity())
    self:SetVelocity(vector_origin)
end

local function IsBSPModel(pEnt)
    if pEnt:GetSolid() == SOLID_BSP then
        return true
    end

    local model = pEnt:GetModel()

    if pEnt:GetSolid() == SOLID_VPHYSICS and model:find("^%*(%d+)") then
        return true
    end

    return false
end

local function IsStandable(pEnt)
    if bit.band(pEnt:GetSolidFlags(), FSOLID_NOT_STANDABLE) ~= 0 then
        return false
    end

    if pEnt:GetSolid() == SOLID_BSP or pEnt:GetSolid() == SOLID_VPHYSICS or pEnt:GetSolid() == SOLID_BBOX then
        return true
    end

    return IsBSPModel(pEnt)
end

-- Purpose: Bounds velocity
local sv_maxvelocity = GetConVar"sv_maxvelocity"
function ENT:PhysicsCheckVelocity()
    local origin = self:Get_Pos()
    local vecAbsVelocity = self:GetFinalVelocity()

    local bReset = false
    for i = 1,3 do
        --[[
        if ( IS_NAN(vecAbsVelocity[i]) )
        {
            Msg( "Got a NaN velocity on %s\n", GetClassname() )
            vecAbsVelocity[i] = 0
            bReset = true
        }
        if ( IS_NAN(origin[i]) )
        {
            Msg( "Got a NaN origin on %s\n", GetClassname() )
            origin[i] = 0
            bReset = true
        }
        ]]

        if vecAbsVelocity[i] > sv_maxvelocity:GetFloat() then
            vecAbsVelocity[i] = sv_maxvelocity:GetFloat()
            bReset = true
        elseif ( vecAbsVelocity[i] < -sv_maxvelocity:GetFloat() ) then
            vecAbsVelocity[i] = -sv_maxvelocity:GetFloat()
            bReset = true
        end
    end

    if bReset then
        self:Set_Pos( origin )
        self:SetFinalVelocity( vecAbsVelocity )
    end
end

local sv_grenade_trajectory = CreateConVar("swcs_debug_grenade_trajectory", "0", {FCVAR_REPLICATED, FCVAR_NOTIFY},"Shows grenade trajectory visualization in-game.")
local sv_grenade_trajectory_thickness = CreateConVar("swcs_debug_grenade_trajectory_thickness", "0.2", {FCVAR_REPLICATED, FCVAR_NOTIFY},"Visible thickness of grenade trajectory arc", 0.1, 1)
local sv_grenade_trajectory_time = CreateConVar("swcs_debug_grenade_trajectory_time", "20", {FCVAR_REPLICATED, FCVAR_NOTIFY},"Length of time grenade trajectory remains visible.",0.1,20)
local sv_grenade_trajectory_dash = CreateConVar("swcs_debug_grenade_trajectory_dash", "0", {FCVAR_REPLICATED, FCVAR_NOTIFY},"Dot-dash style grenade trajectory arc")
local kSleepVelocity = 20
local kSleepVelocitySquared = kSleepVelocity * kSleepVelocity
function ENT:PhysicsToss(phys, dTime)
    local trace = self.m_tTouchTrace
    local move = self:GetFinalVelocity() * dTime

    -- Moving upward, off the ground, or resting on a client/monster, remove FL_ONGROUND
    if self:GetFinalVelocity().z > 0 or self:GetFinalVelocity():LengthSqr() > kSleepVelocitySquared or not self:GetGroundEntity():IsValid() or not IsStandable(self:GetGroundEntity()) then
        self:SetGroundEntity(NULL)
    end

    -- Check to see if entity is on the ground at rest
    if bit.band(self:GetFlags(), FL_ONGROUND) ~= 0 and ( self:GetFinalVelocity():IsZero() ) then
        -- Clear rotation if not moving (even if on a conveyor)
        self:SetLocalAngularVelocity( angle_zero );
        if ( self:GetFinalVelocity():IsZero() ) then
            return end
    end

    self:PhysicsCheckVelocity()

    -- add gravity
    if ( self:GetMoveType() == MOVETYPE_FLYGRAVITY and (bit.band(self:GetFlags(), FL_FLY) == 0) ) then
        self:PhysicsAddGravityMove( move, dTime )
    else
        -- Base velocity is not properly accounted for since this entity will move again after the bounce without
        -- taking it into account
        move:Set(self:GetFinalVelocity())
        move:Mul(dTime)
        self:PhysicsCheckVelocity()
    end

    -- move angles
    --SimulateAngles( dTime )

    -- move origin
    self:PhysicsPushEntity( move, trace )

    --if ( VPhysicsGetObject() )
    --{
    --    VPhysicsGetObject()->UpdateShadow( GetAbsOrigin(), vec3_angle, true, dTime );
    --}

    self:PhysicsCheckVelocity();

    if (SERVER and sv_grenade_trajectory:GetInt() ~= 0 and bit.band(self:GetFlags(), FL_GRENADE) ~= 0 ) then
        local vec3tempOrientation = (trace.HitPos - trace.StartPos)
        local angGrTrajAngles = vec3tempOrientation:Angle()

        local flGrTraThickness = sv_grenade_trajectory_thickness:GetFloat();
        local vec3_GrTrajMin = Vector( 1, -flGrTraThickness, -flGrTraThickness );
        local vec3_GrTrajMax = Vector( vec3tempOrientation:Length(), flGrTraThickness, flGrTraThickness );
        local penis = ( sv_grenade_trajectory_dash:GetInt() ~= 0 and CurTime() % 0.1 < 0.05 );

        -- extruded "line" is really a box for more visible thickness
        debugoverlay.BoxAngles(trace.StartPos, vec3_GrTrajMin, vec3_GrTrajMax, angGrTrajAngles, sv_grenade_trajectory_time:GetFloat(), Color(0, penis and 20 or 200, 0, 255))

        -- per-bounce box
        if (trace.Fraction ~= 1.0) then
            debugoverlay.Box(trace.HitPos, -Vector(SWCS_GRENADE_DEFAULT_SIZE, SWCS_GRENADE_DEFAULT_SIZE, SWCS_GRENADE_DEFAULT_SIZE), Vector(SWCS_GRENADE_DEFAULT_SIZE, SWCS_GRENADE_DEFAULT_SIZE, SWCS_GRENADE_DEFAULT_SIZE), 5, Color(220, 0,0,190))
        end
    end

    -- bonked into something
    if (trace.Fraction ~= 1.0) then
        self:ResolveFlyCollisionCustom( trace, move, dTime );
    end

    -- check for in water
    self:PhysicsCheckWaterTransition();
end

ENT.m_nWaterType = 0
function ENT:SetWaterType(nType)
    self.m_nWaterType = 0

    if bit.band(nType, CONTENTS_WATER) ~= 0 then
        self.m_nWaterType = bit.bor(self.m_nWaterType, 1)
    end

    if bit.band(nType, CONTENTS_SLIME) ~= 0 then
        self.m_nWaterType = bit.bor(self.m_nWaterType, 2)
    end
end
function ENT:GetWaterType()
    local out = 0

    if bit.band(self.m_nWaterType, 1) ~= 0 then
        out = bit.bor(out, CONTENTS_WATER)
    end

    if bit.band(self.m_nWaterType, 2) ~= 0 then
        out = bit.bor(out, CONTENTS_SLIME)
    end

    return out
end

function ENT:PhysicsCheckWaterTransition()
    local oldcont = self:GetWaterType()
    self:UpdateWaterState()
    local cont = self:GetWaterType()

    if self:GetMoveParent():IsValid() then
        return
    end

    if bit.band(cont, MASK_WATER) ~= 0 then
        if oldcont == CONTENTS_EMPTY then
            if CLIENT then
                -- splash effect
            end

            -- just crossed into water
            self:EmitSound( "BaseEntity.EnterWater" )

            if not self:IsEFlagSet(EFL_NO_WATER_VELOCITY_CHANGE) then
                local vecAbsVelocity = self:GetFinalVelocity()
                vecAbsVelocity.z = vecAbsVelocity.z * 0.5
                self:SetFinalVelocity(vecAbsVelocity)
            end
        end
    elseif oldcont ~= CONTENTS_EMPTY then
        -- just crossed out of water
        self:EmitSound( "BaseEntity.ExitWater" )
    end
end

function ENT:UpdateWaterState()
    -- Compute the point to check for water state
    local point = self:WorldSpaceCenter()

    self:SetWaterLevel(0)
    self:SetWaterType(CONTENTS_EMPTY)
    local cont = util.PointContents(point)

    if bit.band(cont, MASK_WATER) == 0 then
        return
    end

    self:SetWaterType(cont)
    self:SetWaterLevel(1)

    -- point sized entities are always fully submerged
    if self:BoundingRadius() == 0 then
        self:SetWaterLevel(3)
    else
        -- Check the exact center of the box
        point.z = self:WorldSpaceCenter().z

        local midcont = util.PointContents(point)
        if bit.band(midcont, MASK_WATER) ~= 0 then
            -- Now check where the eyes are...
            self:SetWaterLevel(2)

            local eyecont = util.PointContents(self:EyePos())
            if bit.band(eyecont, MASK_WATER) ~= 0 then
                self:SetWaterLevel(3)
            end
        end
    end
end

function ENT:GetImpactDamage()
    return 2
end

function ENT:ResolveFlyCollisionCustom(trace, vecMove, dTime)
    local pEntity = trace.Entity

    -- this is necessary so that we can hit the world
    if not pEntity:IsValid() and not pEntity:IsWorld() then
        return end

    -- if its breakable glass and we kill it, don't bounce.
    -- give some damage to the glass, and if it breaks, pass 
    -- through it.
    local breakthrough = false

    if pEntity:GetClass() == "func_breakable" then
        breakthrough = true
    end

    if pEntity:GetClass() == "func_breakable_surf" then
        breakthrough = true
    end

    local m_takedamage = pEntity:GetInternalVariable("m_takedamage")
    if pEntity:GetClass() == "prop_physics_multiplayer" and pEntity:GetMaxHealth() > 0 and m_takedamage == 2 then
        breakthrough = true
    end

    -- this one is tricky because BounceTouch hits breakable propers before we hit this function and the damage is already applied there (CBaseGrenade::BounceTouch( CBaseEntity *ppEntity ))
    -- by the time we hit this, the prop hasn't been removed yet, but it broke, is set to not take anymore damage and is marked for deletion - we have to cover this case here
    if pEntity:GetClass() == "prop_dynamic" and pEntity:GetMaxHealth() > 0 and (m_takedamage == 2 or (m_takedamage == 0 and pEntity:IsEFlagSet( EFL_KILLME ))) then
        breakthrough = true
    end

    if breakthrough then
        if SERVER then
            local info = DamageInfo()
            info:SetInflictor(self)
            info:SetAttacker(self:GetOwner():IsValid() and self:GetOwner() or self)
            info:SetDamage(10)
            info:SetDamageType(DMG_CLUB)
            pEntity:DispatchTraceAttack(info, trace, self:GetFinalVelocity():GetNormalized())
        end

        if pEntity:Health() <= 0 then
            -- slow our flight a little bit
            local vel = self:GetFinalVelocity()

            vel:Mul(0.4)

            self:SetFinalVelocity( vel )
            return
        end
    end

    --Assume all surfaces have the same elasticity
    local flSurfaceElasticity = 1.0

    --Don't bounce off of players with perfect elasticity
    if pEntity:IsPlayer() then
        flSurfaceElasticity = 0.3

        -- and do slight damage to players on the opposite team
        if SERVER then
            local dmg = DamageInfo()
            dmg:SetAttacker(self:GetOwner():IsValid() and self:GetOwner() or self)
            dmg:SetInflictor(self)
            dmg:SetDamage(self:GetImpactDamage())
            dmg:SetDamageType(DMG_GENERIC)
            dmg:SetReportedPosition(self:GetPos())
            dmg:SetDamagePosition(self:GetPos())
            pEntity:TakeDamageInfo(dmg)
        end
    end

    --Don't bounce twice on a selection of problematic entities
    local bIsProjectile = pEntity.IsSWCSGrenade == true--dynamic_cast< CBaseCSGrenadeProjectile* >( pEntity ) != NULL;
    if not pEntity:IsWorld() and self.m_lastHitPlayer == pEntity then
        local bIsHostage = false--dynamic_cast< CHostage* >( pEntity ) != NULL;

        if pEntity:IsPlayer() or bIsHostage or bIsProjectile then
            self:SetActualCollisionGroup( COLLISION_GROUP_DEBRIS )
            if ( bIsProjectile ) then
                self:SetActualCollisionGroup( COLLISION_GROUP_DEBRIS )
            end

            return
        end
    end

    self.m_lastHitPlayer = pEntity

    local flTotalElasticity = self:GetGrenadeElasticity() * flSurfaceElasticity
    flTotalElasticity = math.Clamp( flTotalElasticity, 0.0, 0.9 )

    -- NOTE: A backoff of 2.0f is a reflection
    local vecAbsVelocity = Vector()
    self:PhysicsClipVelocity( self:GetFinalVelocity(), trace.HitNormal, vecAbsVelocity, 2 )
    vecAbsVelocity:Mul(flTotalElasticity)
    self:SetFinalVelocity(vecAbsVelocity)

    -- Get the total velocity (player + conveyors, etc.)
    vecMove:Set(vecAbsVelocity) -- + self:GetBaseVelocity())
    local flSpeedSqr = vecMove:LengthSqr()

    local bIsWeapon = pEntity:IsWeapon()

    local hCollisionEntity = trace.Entity
    local tSavedTouchTrace = table.Copy(trace)
    -- Stop if on ground or if we bounce and our velocity is really low (keeps it from bouncing infinitely)
    if ( ( trace.HitNormal.z > 0.7 ) or (trace.HitNormal.z > 0.1 and flSpeedSqr < kSleepVelocitySquared) ) and
        ( IsStandable(pEntity) or bIsProjectile or bIsWeapon or pEntity:IsWorld() )
    then
        -- clip it again to emulate old behavior and keep it from bouncing up like crazy when you throw it at the ground on the first toss
        if ( flSpeedSqr > 96000 ) then
            local alongDist = vecAbsVelocity:GetNormalized():Dot(trace.HitNormal)
            if ( alongDist > 0.5 ) then
                local flBouncePadding = (1.0 - alongDist) + 0.5
                vecAbsVelocity:Mul(flBouncePadding)
            end
        end

        self:SetFinalVelocity( vecAbsVelocity )

        if ( flSpeedSqr < kSleepVelocitySquared ) then
            -- stop moving

            self:SetGroundEntity( pEntity )
            self:SetNWMoveType(MOVETYPE_NONE)

            -- Reset velocities.
            self:SetVelocity( vector_origin )
            self:SetFinalVelocity( vector_origin )
            self:SetLocalAngularVelocity( angle_zero )

            --align to the ground so we're not standing on end
            local angle = trace.HitNormal:Angle()

            -- rotate randomly in yaw
            angle:RotateAroundAxis(angle:Forward(), g_ursRandom:RandomFloat( 0, 360 ))
            --angle[2] = g_ursRandom:RandomFloat( 0, 360 )

            -- TODO: rotate around trace.plane.normal

            self:SetAngles(angle)
        else
            -- bounce off floor

            local vecBaseDir = self:GetFinalVelocity()
            if ( not vecBaseDir:IsZero() ) then
                vecBaseDir:Normalize()
                local vecDelta = self:GetFinalVelocity() - vecAbsVelocity
                local flScale = vecDelta:LengthSqr(vecBaseDir)
                vecAbsVelocity:Add(self:GetFinalVelocity() * flScale)
            end

            vecMove:Set(vecAbsVelocity)
            vecMove:Mul((1 - trace.Fraction) * dTime)

            self:PhysicsPushEntity( vecMove, trace )

            self:SetFinalVelocity(vecAbsVelocity)
        end
    else
        -- bounce off wall

        self:SetFinalVelocity(vecAbsVelocity)
        vecMove:Set(vecAbsVelocity)
        vecMove:Mul(trace.Fraction)
        vecMove:Mul(dTime)

        self:PhysicsPushEntity( vecMove, trace )
    end

    local hOurPhys = self:GetPhysicsObject()
    local hOtherPhys = pEntity:GetPhysicsObject()
    if hOtherPhys:IsValid() then
        -- The impulse to be applied in kg*source_unit/s. (World frame)

        local flImpulse = hOurPhys:GetMass() * self:GetFinalVelocity():Length()
        local vImpulse = trace.Normal * -flImpulse

        hOtherPhys:ApplyForceOffset(vImpulse, trace.HitPos)
    end

    self:SetAbsVelocity(self:GetFinalVelocity())
    if IsFirstTimePredicted() then
        self:BounceSound()
    end
    self:OnBounced(tSavedTouchTrace, hCollisionEntity)

    if self:GetBounces() > SWCS_GRENADE_FAILSAFE_MAX_BOUNCES then
        -- failsafe detonate after 20 bounces
        self:SetVelocity(vector_origin)
        self:SetFinalVelocity(vector_origin)
        self:DetonateOnNextThink()
        self:SetNWMoveType(MOVETYPE_NONE)
    else
        self:SetBounces(self:GetBounces() + 1)
    end
end

local function UTIL_TraceEntity( pEntity, vecAbsStart, vecAbsEnd, mask, pTr )
    util.TraceEntity({
        start = vecAbsStart,
        endpos = vecAbsEnd,
        filter = {pEntity, pEntity:GetOwner()},
        output = pTr,
        mask = mask
    }, pEntity)
end

local function Physics_TraceEntity( pBaseEntity, vecAbsStart,
    vecAbsEnd, mask, pTr )

    --if (pBaseEntity->GetDamageType() != DMG_GENERIC) then
    --    GameRules()->WeaponTraceEntity( pBaseEntity, vecAbsStart, vecAbsEnd, mask, ptr );
    --else
        UTIL_TraceEntity( pBaseEntity, vecAbsStart, vecAbsEnd, mask, pTr );

        -- perform an additional trace if this is a grenade projectile hitting a player
        --CBaseCSGrenadeProjectile* pGrenadeProjectile = dynamic_cast<CBaseCSGrenadeProjectile*>( pBaseEntity );

        if pBaseEntity.IsSWCSGrenade and pTr.StartSolid and bit.band(pTr.Contents, CONTENTS_GRENADECLIP) ~= 0 then
            -- HACK HACK: players don't collide with CONTENTS_GRENADECLIP, so it's possible (but very inadvisable) for maps to contain
            -- CONTENTS_GRENADECLIP brushes that are big enough for the player to throw a grenade from INSIDE one. To account for this
            -- in the simplest and most straightforward way, I'm just running the trace again to let grenades fly OUT of CONTENTS_GRENADECLIP
            -- volumes, just not INTO them.
            --UTIL_ClearTrace( *pTr )
            UTIL_TraceEntity( pBaseEntity, vecAbsStart, vecAbsEnd, bit.band(mask, bit.bnot(CONTENTS_GRENADECLIP)), pTr )
        end

        -- fraction < 1 || allsolid || startsolid
        if pBaseEntity.IsSWCSGrenade and (pTr.Fraction < 1 or pTr.AllSolid or pTr.StartSolid ) and pTr.Entity:IsValid() and pTr.Entity:IsPlayer() then
            --UTIL_ClearTrace( *pTr );
            --why does traceline respect hitmoxes in the mask param but traceentity and tracehull do not?
            util.TraceLine({
                start = vecAbsStart,
                endpos = vecAbsEnd,
                mask = mask,
                filter = pBaseEntity,
                collisiongroup = pBaseEntity:GetActualCollisionGroup(),
                output = pTr
            })
        end
    --end
end

local function PhysicsCheckSweep(pEntity, vecAbsStart, vecAbsDelta, pTrace)
    local mask = MASK_SOLID -- pEntity:GetSolidMask()

    local vecAbsEnd = vecAbsStart + vecAbsDelta

    -- Set collision type
    if not pEntity:IsSolid() or bit.band(pEntity:GetSolidFlags(), FSOLID_VOLUME_CONTENTS) ~= 0 then
        if pEntity:GetMoveParent():IsValid() then
            -- UTIL_ClearTrace( *pTrace )
            --table.Empty(pTrace)

            return
        end

        -- don't collide with monsters
        mask = bit.band(mask, bit.bnot(CONTENTS_MONSTER))
    end

    Physics_TraceEntity( pEntity, vecAbsStart, vecAbsEnd, mask, pTrace )
end

function ENT:PhysicsPushEntity(push, pTrace)
    if self:GetMoveParent():IsValid() then
        return
    end

    -- NOTE: absorigin and origin must be equal because there is no moveparent
    local prevOrigin = self:Get_Pos()

    PhysicsCheckSweep(self, prevOrigin, push, pTrace)

    -- if the sweep check starts inside a solid surface, try once more from the last origin
    if pTrace.StartSolid then
        self:SetActualCollisionGroup(COLLISION_GROUP_INTERACTIVE_DEBRIS)
        util.TraceLine({
            start = prevOrigin - push,
            endpos = prevOrigin + push,
            mask = bit.bor(CONTENTS_SOLID, CONTENTS_MOVEABLE, CONTENTS_WINDOW, CONTENTS_GRATE),
            filter = self,
            collisiongroup = self:GetActualCollisionGroup(),
            output = pTrace
        })
    end

    if pTrace.Fraction ~= 0 then
        self:Set_Pos(pTrace.HitPos)
    end

    -- Passing in the previous abs origin here will cause the relinker
    -- to test the swept ray from previous to current location for trigger intersections
    --PhysicsTouchTriggers( &prevOrigin );

    if pTrace.Entity:IsValid() then
        --PhysicsImpact( pTrace->m_pEnt, *pTrace );
    end
end

local STOP_EPSILON = 0.1
function ENT:PhysicsClipVelocity( vin, normal, out, overbounce )
    local backoff
    local change
    local angle
    local blocked

    blocked = 0

    angle = normal[ 3 ]

    if angle > 0 then
        blocked = bit.bor(blocked, 1) -- floor
    end
    if angle == 0 then
        blocked = bit.bor(blocked, 2) -- step
    end

    backoff = vin:Dot(normal) * overbounce

    for i = 1, 3 do
        change = normal[i] * backoff
        out[i] = vin[i] - change
        if (out[i] > -STOP_EPSILON and out[i] < STOP_EPSILON) then
            out[i] = 0
        end
    end

    return blocked
end

local sv_gravity = GetConVar"sv_gravity"
local function GetActualGravity(pEnt)
    local ent_gravity = pEnt:GetGravity()
    if ent_gravity == 0 then
        ent_gravity = 1
    end

    -- we want sv_gravity to be 800 for grenades
    -- but gmod's default is 600
    -- so just fudge it
    local flSvGravityValue = sv_gravity:GetFloat()
    if flSvGravityValue == 600 then
        flSvGravityValue = 800
    end

    return flSvGravityValue * ent_gravity
end

function ENT:PhysicsAddGravityMove( move, dTime )
    local vecAbsVelocity = self:GetFinalVelocity()

    move.x = (vecAbsVelocity.x + self:GetBaseVelocity().x ) * dTime
    move.y = (vecAbsVelocity.y + self:GetBaseVelocity().y ) * dTime

    if ( bit.band(self:GetFlags(), FL_ONGROUND) ~= 0 ) then
        move.z = self:GetBaseVelocity().z * dTime
        return
    end

    -- linear acceleration due to gravity
    local newZVelocity = vecAbsVelocity.z - GetActualGravity( self ) * dTime

    move.z = (((vecAbsVelocity.z + newZVelocity) * 0.5) + self:GetBaseVelocity().z ) * dTime

    --local vecBaseVelocity = self:GetBaseVelocity()
    --vecBaseVelocity.z = 0.0
    --self:SetBaseVelocity( vecBaseVelocity )

    vecAbsVelocity.z = newZVelocity
    self:SetFinalVelocity( vecAbsVelocity )
    --self:SetAbs-Velocity( vecAbsVelocity )

    -- Bound velocity
    self:PhysicsCheckVelocity()
end
