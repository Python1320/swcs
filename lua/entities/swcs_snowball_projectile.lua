AddCSLuaFile()

ENT.Base = "baseswcsgrenade_projectile"
ENT.m_flTimeToDetonate = 99

DEFINE_BASECLASS(ENT.Base)

function ENT:Create(pos, angs, vel, angvel, owner)
    self:SetPos(pos)
    self:SetAngles(angs)

    self:SetVelocity(vel)
    self:SetInitialVelocity(vel)

    if IsValid(owner) then
        self:SetThrower(owner)
        self:SetOwner(owner)
    end

    self:SetDetonateTimerLength(self.m_flTimeToDetonate)

    self:SetLocalAngularVelocity(angvel)
    self:SetFinalAngularVelocity(angvel)
    self:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)

    -- we have to reset these here because we set the model late and it resets the collision
    local min = Vector( -SWCS_GRENADE_DEFAULT_SIZE, -SWCS_GRENADE_DEFAULT_SIZE, -SWCS_GRENADE_DEFAULT_SIZE )
    local max = Vector( SWCS_GRENADE_DEFAULT_SIZE, SWCS_GRENADE_DEFAULT_SIZE, SWCS_GRENADE_DEFAULT_SIZE )
    self:SetCollisionBounds(min, max)

    return self
end

function ENT:Initialize()
    self:SetModel("models/weapons/csgo/w_eq_snowball_dropped.mdl")

    BaseClass.Initialize(self)

    if CLIENT then
        self:CreateParticleEffect("weapon_snowball_trail", -1)
    end
end

function ENT:OnBounced(trace, other)
    if other:IsFlagSet(bit.bor(FSOLID_TRIGGER, FSOLID_VOLUME_CONTENTS)) then
        return end

    if other == self:GetOwner() then
        return end

    local class = other:GetClass()
    if class == "func_breakable" or class == "func_breakable_surf" or class == "func_ladder" then
        return end

    if SERVER then
        swcs.SendParticle("weapon_snowball_impact", trace.HitPos, trace.HitNormal:Angle())
        swcs.SendParticle("snow_hit_player_screeneffect", trace.HitPos, trace.HitNormal:Angle())
    end

    if other:IsValid() and (other:IsNPC() or other:IsPlayer() or other:IsNextBot()) then
        if other:IsPlayer() then
            other:EmitSound("Snowball.HitPlayerFace")
        end
    else
        if SERVER or (CLIENT and IsFirstTimePredicted()) then
            self:EmitSound("Snowball.Impact")
        end

        if SERVER then
            swcs.SendParticle("weapon_snowball_impact_stuck_wall", trace.HitPos, trace.HitNormal:Angle())
            swcs.SendParticle("weapon_snowball_impact_splat", trace.HitPos, trace.HitNormal:Angle())
        end
    end

    if SERVER then SafeRemoveEntity(self) end
end

function ENT:GetImpactDamage()
    return 5
end
