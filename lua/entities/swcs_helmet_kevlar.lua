AddCSLuaFile()
ENT.Base = "swcs_base_item"
ENT.Category = "CS:GO"
ENT.Spawnable = true
ENT.Model = Model("models/weapons/csgo/w_eq_armor_helmet.mdl")

function ENT:CanInteract(actor)
    if actor:Armor() < 100 or not actor:HasHelmet() then
        return true
    end

    return false
end

function ENT:OnInteract(actor)
    if actor:Armor() < 100 then
        actor:SetArmor(100)
    end
    if not actor:HasHelmet() then
        actor:GiveHelmet()
    end
end

