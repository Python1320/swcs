include("shared.lua")

local TAG = "swcs_inferno"

ENT.m_NetworkTimer = swcs.CountdownTimer()
ENT.m_PVSPlayers = {}
-- called every :Think() while the inferno is burning
function ENT:NetworkAllFire()
    if not self.m_NetworkFilter then
        self.m_NetworkFilter = RecipientFilter()
    end

    if not self.m_NetworkTimer:IsElapsed() then
        return end

    self.m_NetworkTimer:Start(0.25)

    -- populate filter
    local PlayersList = self.m_NetworkFilter:GetPlayers()

    self.m_NetworkFilter:AddPVS(self:GetPos())

    local NewPlayersList = self.m_NetworkFilter:GetPlayers()

    local Players, NewPlayers = {}, {}

    for i = 1, #PlayersList do
        Players[PlayersList[i]] = true
    end

    for i = 1, #NewPlayersList do
        NewPlayers[NewPlayersList[i]] = true
    end

    for ply in next, NewPlayers do
        -- send full update to new players in PVS
        if not Players[ply] then
            net.Start(TAG, false)
                net.WriteEntity(self)
                net.WriteBool(true) -- this is a full update

                net.WriteUInt(self:GetFireCount(), 6) -- expected amount

                for i = 0, self:GetFireCount() - 1 do
                    local fire = self.m_fire[i]

                    net.WriteBool(fire and true or false)
                    if not fire then continue end

                    net.WriteFloat(self.m_fireXDelta[i])
                    net.WriteFloat(self.m_fireYDelta[i])
                    net.WriteFloat(self.m_fireZDelta[i])
                    net.WriteBool(self.m_bFireIsBurning[i])
                    net.WriteNormal(self.m_BurnNormal[i])
                end

            net.Send(ply)
        end
    end
end
