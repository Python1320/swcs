include("shared.lua")

local TAG = "swcs_inferno"

local InfernoDebug = GetConVar("inferno_debug")
local InfernoDlightSpacing = CreateClientConVar( "inferno_dlight_spacing", "200", nil, nil, "Inferno dlights are at least this far apart" )
local InfernoDlights = CreateClientConVar( "inferno_dlights", "30", nil, nil, "Min FPS at which molotov dlights will be created" )
local InfernoFire = CreateClientConVar( "inferno_fire", "2" )

net.Receive(TAG, function()
    local ent = net.ReadEntity()

    -- not valid somehow?
    if not ent:IsValid() then return end

    -- what the fuck?
    if not ent.m_fireXDelta then return end

    local bFullUpdate = net.ReadBool()

    if bFullUpdate then
        local iFireCount = net.ReadUInt(6)

        for i = 0, iFireCount - 1 do
            local ok = net.ReadBool()
            if not ok then continue end

            ent.m_fireXDelta[i] = net.ReadFloat()
            ent.m_fireYDelta[i] = net.ReadFloat()
            ent.m_fireZDelta[i] = net.ReadFloat()
            ent.m_bFireIsBurning[i] = net.ReadBool()
            ent.m_BurnNormal[i] = net.ReadNormal()
        end
    else
        local iFire = net.ReadUInt(6)

        ent.m_fireXDelta[iFire] = net.ReadFloat()
        ent.m_fireYDelta[iFire] = net.ReadFloat()
        ent.m_fireZDelta[iFire] = net.ReadFloat()
        ent.m_bFireIsBurning[iFire] = net.ReadBool()
        ent.m_BurnNormal[iFire] = net.ReadNormal()
    end
end)

ENT.m_drawable = {}
ENT.m_drawableCount = 0
ENT.m_lastFireCount = 0

local OLD_FIRE_MASK = 1
local NEW_FIRE_MASK = 2

local FIRE_STATE_STARTING = 0
local FIRE_STATE_BURNING = 1
local FIRE_STATE_GOING_OUT = 2
local FIRE_STATE_FIRE_OUT = 3
local FIRE_STATE_UNKNOWN = 4

AccessorFunc(ENT, "m_lastFireCount", "LastFireCount", FORCE_NUMBER)
AccessorFunc(ENT, "m_drawableCount", "DrawableCount", FORCE_NUMBER)

-- Monitor changes and recompute render bounds
function ENT:ClientThink()
    local bIsAttachedToMovingObject = self:GetMoveParent():IsValid()

    if self:GetLastFireCount() ~= self:GetFireCount() or bIsAttachedToMovingObject then
        self:SynchronizeDrawables()
        self:SetLastFireCount(self:GetFireCount())
    end

    local bDidRecomputeBounds = false

    -- update Drawables
    for i = 0, self:GetDrawableCount() - 1 do
        local draw = self.m_drawable[i]
        if not draw then continue end

        if draw.m_state == FIRE_STATE_STARTING then
            local growRate = draw.m_maxSize / 2
            draw.m_size = growRate * (RealTime() - draw.m_stateTimestamp)

            if draw.m_size > draw.m_maxSize then
                draw.m_size = draw.m_maxSize
                draw:SetState(FIRE_STATE_BURNING)
            end
        elseif draw.m_state == FIRE_STATE_GOING_OUT then
            local dieRate = draw.m_maxSize / 2
            draw.m_size = draw.m_maxSize - dieRate * (RealTime() - draw.m_stateTimestamp)

            if draw.m_size <= 0 then
                draw:SetState(FIRE_STATE_FIRE_OUT)

                self:RecomputeBounds()
                bDidRecomputeBounds = true
            end
        end
    end

    if bIsAttachedToMovingObject and not bDidRecomputeBounds then
        self:RecomputeBounds()
    end

    self:UpdateParticles()
end

function ENT:SynchronizeDrawables()
    -- mark all fires as "burning" as "unknown" - active ones will be reset
    for i = 0, self:GetDrawableCount() - 1 do
        local fire = self.m_drawable[i]
        if not fire then continue end

        if fire.m_state == FIRE_STATE_BURNING then
            fire.m_state = FIRE_STATE_UNKNOWN
        end
    end

    local vInfernoOrigin = self:GetPos()

    for i = 0, self:GetFireCount() - 1 do
        local firePos = Vector(vInfernoOrigin)

        local vecFlamePos = Vector(self.m_fireXDelta[i], self.m_fireYDelta[i], self.m_fireZDelta[i])

        firePos:Add(vecFlamePos)

        local fireNormal = self.m_BurnNormal[i]
        local info = self:GetDrawable(firePos)

        if self.m_bFireIsBurning[i] == false then
            if info and info.m_state ~= FIRE_STATE_FIRE_OUT then
                info.m_state = FIRE_STATE_FIRE_OUT

                -- render bounds changed
                self:RecomputeBounds()
            end

            continue
        elseif info then
            -- existing fire continues to burn
            if info.m_state == FIRE_STATE_UNKNOWN then
                info.m_state = FIRE_STATE_BURNING
            end
        else
            -- new fire
            local info = {
                m_pos = firePos,					    -- < position of flame
                m_normal = fireNormal,				    -- < normal of flame surface
                m_frame = 0,						    -- < current animation frame
                m_framerate = 0,					    -- < rate of animation
                m_mirror = false,					    -- < if true, flame is mirrored about vertical axis

                m_dlightIndex = 0,

                m_state = FIRE_STATE_STARTING,		    -- < the state of this fire
                m_stateTimestamp = 0,				    -- < when the fire entered its current state
                SetState = function( fire, state )
                    fire.m_state = state
                    fire.m_stateTimestamp = RealTime()
                end,

                m_size = 0,							    -- < current flame size
                m_maxSize = 0,						    -- < maximum size of full-grown flame

                Draw = function()						-- < render this flame
                end
            }
            self.m_drawable[i] = info

            info:SetState(FIRE_STATE_STARTING)
            info.m_framerate = g_ursRandom:RandomFloat(0.04, 0.06)
            info.m_mirror = g_ursRandom:RandomFloat(0, 100) < 50
            info.m_maxSize = g_ursRandom:RandomFloat(70, 90)

            local closeDlight = false

            if closeDlight then
                info.m_dlightIndex = 0
            else
                info.m_dlightIndex = self:EntIndex() + self:GetDrawableCount()
            end

            self:RecomputeBounds()

            self:SetDrawableCount(self:GetDrawableCount() + 1)
        end
    end

    -- any fires still in the UNKNOWN state are now GOING_OUT
    for i = 0, self:GetDrawableCount() - 1 do
        local fire = self.m_drawable[i]
        if not fire then continue end

        if fire.m_state == FIRE_STATE_UNKNOWN then
            fire:SetState(FIRE_STATE_GOING_OUT)
        end
    end
end

local function VectorsAreEqual(a,b, tolerance)
    if math.abs(a.x - b.x) > tolerance then
        return false end
    if math.abs(a.y - b.y) > tolerance then
        return false end

    return math.abs(a.z - b.z) <= tolerance
end

function ENT:GetDrawable(pos)
    local equalTolerance = 12
    for i = 0, self:GetDrawableCount() - 1 do
        local fire = self.m_drawable[i]
        if not fire then continue end

        if VectorsAreEqual(fire.m_pos, pos, equalTolerance) then
            fire.m_pos:Set(pos)
            return fire
        end
    end
end

ENT.m_burnParticleEffect = NULL
function ENT:UpdateParticles()
    if self:GetDrawableCount() > 0 and bit.band(InfernoFire:GetInt(), NEW_FIRE_MASK) ~= 0 then
        if not (self.m_burnParticleEffect and self.m_burnParticleEffect:IsValid()) then
            self.m_burnParticleEffect = CreateParticleSystem(self, self:GetParticleEffectName(), PATTACH_ABSORIGIN_FOLLOW)
        else
            for i = 0, self:GetDrawableCount() - 1 do
                local draw = self.m_drawable[i]
                if not draw then continue end

                if draw.m_state >= FIRE_STATE_FIRE_OUT then
                    local vecCenter = Vector(draw.m_pos)
                    vecCenter.z = vecCenter.z - 9999
                    draw.m_pos:Set(vecCenter)
                    draw.m_size = 0

                    -- this sucks
                    if i ~= 0 then
                        --self.m_burnParticleEffect:SetControlPoint( i, Vector(math.huge, math.huge, math.huge) )
                    end
                else
                    --self.m_burnParticleEffect:SetControlPointEntity(i, self)
                    self.m_burnParticleEffect:SetControlPoint(i, draw.m_pos)

                    if i % 2 == 0 then
                        -- Elight, for perf reasons only for every other fire
                    end
                end
            end

            self:SetNextClientThink(0.1)

            self.m_burnParticleEffect:SetSortOrigin(self:GetRenderOrigin())

            if InfernoDebug:GetBool() then
                local min, max = self:GetRenderBounds()
                debugoverlay.Cross(self:GetRenderOrigin(), 5, 0.01, Color(255,0,255))
                debugoverlay.Box(self:GetRenderOrigin(), min, max, 0.01, Color(255, 0, 255, 16))
            end
        end
    else
        if self.m_burnParticleEffect and self.m_burnParticleEffect:IsValid() then
            self.m_burnParticleEffect:StopEmission()
        end
    end
end

ENT.m_maxFireHalfWidth = 30.0
ENT.m_maxFireHeight = 80.0
ENT.m_minBounds = Vector()
ENT.m_maxBounds = Vector()
function ENT:RecomputeBounds()
    local minBounds = self:GetPos() + Vector( 64.9, 64.9, 64.9 )
    local maxBounds = self:GetPos() + Vector( -64.9, -64.9, -64.9 )

    for i = 0, self:GetDrawableCount() do
        local draw = self.m_drawable[i]
        if not draw then
            continue end

        if draw.m_state == FIRE_STATE_FIRE_OUT then
            continue end

        if (draw.m_pos.x - self.m_maxFireHalfWidth < minBounds.x) then
            minBounds.x = draw.m_pos.x - self.m_maxFireHalfWidth
        end

        if (draw.m_pos.x + self.m_maxFireHalfWidth > maxBounds.x) then
            maxBounds.x = draw.m_pos.x + self.m_maxFireHalfWidth
        end

        if (draw.m_pos.y - self.m_maxFireHalfWidth < minBounds.y) then
            minBounds.y = draw.m_pos.y - self.m_maxFireHalfWidth
        end

        if (draw.m_pos.y + self.m_maxFireHalfWidth > maxBounds.y) then
            maxBounds.y = draw.m_pos.y + self.m_maxFireHalfWidth
        end

        if (draw.m_pos.z < minBounds.z) then
            minBounds.z = draw.m_pos.z
        end

        if (draw.m_pos.z + self.m_maxFireHeight > maxBounds.z) then
            maxBounds.z = draw.m_pos.z + self.m_maxFireHeight
        end
    end

    self:SetRenderOrigin(self:GetPos())
    self:SetRenderBoundsWS(minBounds, maxBounds)
end

local AverageFPS = -1
local high = -1
local low = -1
local NewWeight = 0.1

local mat = Material("sprites/fire1.vmt")
function ENT:DrawFire(fire)
    local halfWidth = fire.m_size / 3.0

    local right = fire.m_mirror and -EyeAngles():Right() or EyeAngles():Right()
    local top = fire.m_pos + (vector_up * fire.m_size)
    local bottom = fire.m_pos

    local tr = top + (right * halfWidth)
    local br = bottom + (right * halfWidth)
    local bl = bottom - (right * halfWidth)
    local tl = top - (right * halfWidth)

    render.SetMaterial(mat)
    mesh.Begin(MATERIAL_QUADS, 2)
    mesh.Quad(bl, br, tr, tl)
    mesh.End()

    debugoverlay.Box(fire.m_pos, -Vector(2,2,2), Vector(2,2,2), 0.1)

    if fire.m_dlightIndex > 0 and InfernoDlights:GetInt() >= 1 then
        local realFrameTime = RealFrameTime()
        if realFrameTime > 2 then
            realFrameTime = -1
        end
        if realFrameTime > 0 then
            local nFps = -1
            local NewFrame = 1.0 / realFrameTime

            if AverageFPS < 0 then
                AverageFPS = NewFrame
                high = math.floor(AverageFPS)
                low = math.floor(AverageFPS)
            else
                AverageFPS = AverageFPS * (1.0 - NewWeight)
                AverageFPS = AverageFPS + (NewFrame * NewWeight)
            end

            local NewFrameInt = math.floor(NewFrame)
            if NewFrameInt < low then NewFrameInt = low end
            if NewFrameInt > high then NewFrameInt = high end

            nFps = math.floor(AverageFPS)
            if nFps < InfernoDlights:GetInt() then
                fire.m_dlightIndex = 0
                return
            end
        end

        -- These are the dlight params from the Ep1 fire glows, with a slightly larger flicker
        -- (radius delta is larger, starting from 250 instead of 400).
        local scale = fire.m_size / fire.m_maxSize * 1.5

        local el = DynamicLight(fire.m_dlightIndex, true)
        local pos = Vector(bottom)
        pos.z = pos.z + (16.0 * scale)

        el.pos = pos
        el.r = 255
        el.g = 100
        el.b = 10
        el.brightness = 3
        el.size = g_ursRandom:RandomFloat(50, 131) * scale
        el.dietime = CurTime() + 0.1
    end
end

function ENT:Draw(a)
    if bit.band(InfernoFire:GetInt(), OLD_FIRE_MASK) == 0 then return end

    for i = 0, self:GetDrawableCount() - 1 do
        local fire = self.m_drawable[i]
        if not fire then continue end

        local frame = math.floor(RealTime() / fire.m_framerate) % mat:GetTexture("$basetexture"):GetNumAnimationFrames()
        mat:SetInt("$frame", frame)

        self:DrawFire(fire)
    end
end
